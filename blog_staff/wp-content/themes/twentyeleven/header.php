<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html lang="ja">

<head>
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="-1" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="ja" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<title>株式会社進和不動産</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="" />
	<meta property="og:title" content="" />
	<meta property="og:description" content="" />
	<link href="../common/css/index.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../common/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="../common/js/main.js"></script>
</head>

<body>
	<div id="wrap">
		<div id="stage">
			<div id="pagecontent">
				<div id="pagecontent_wrap">
					<div id="header">
						<h1>
							<a id="header_logo" href="../index.html"><span class="hide_text">株式会社進和不動産</span></a>
						</h1>
						<div id="header_inquiry">
							<a id="header_inquiry_btn" href="../inquiry/index.html"><span class="hide_text">フォームでのお問い合わせ</span></a>
						</div><!-- /div#header_inquiry -->
					</div><!-- /div#header -->
					<div id="navi">
						<div id="navi_menu" class="clearfix">
							<a id="navi_menu_item_home" href="../index.html"><span class="hide_text">HOME</span></a>
							<div class="navi_menu_separator"></div>
							<a id="navi_menu_item_special" href="../bukken/"><span class="hide_text">今月の特集</span></a>
							<div class="navi_menu_separator"></div>
							<a id="navi_menu_item_event" href="../event/index.html"><span class="hide_text">イベント・セミナー情報</span></a>
							<div class="navi_menu_separator"></div>
							<a id="navi_menu_item_investment" href="../investment/index.html"><span class="hide_text">失敗しない不動産投資</span></a>
							<div class="navi_menu_separator"></div>
							<a id="navi_menu_item_consultant" href="../consultant/index.html"><span class="hide_text">コンサルタント紹介</span></a>
							<div class="navi_menu_separator"></div>
							<a id="navi_menu_item_present" href="../present/index.html"><span class="hide_text">小冊子プレゼント</span></a>
						</div><!-- /div#navi_menu -->
						<div id="navi_display" class="clearfix"><span class="hide_text">進和不動産は、不動産業界の常識と断固戦います。</span></div><!-- /div#navi_display -->
					</div><!-- /div#navi -->
					<div id="board" class="clearfix">
						<div id="column_left">
							<a id="column_left_member_btn" href="../regist/index.html"><span class="hide_text">会員登録</span></a>
							<div id="column_left_menu">
								<div id="column_left_menu_title"></div><!-- /div#column_left_menu_title -->
								<a id="column_left_menu_item_shuueki" href="../bukken/"><span class="hide_text">収益物件を探す</span></a>
								<a id="column_left_menu_item_consult" href="../freeconsultation/index.html"><span class="hide_text">無料相談</span></a>
								<a id="column_left_menu_item_company" href="../company/index.html"><span class="hide_text">会社概要</span></a>
								<a id="column_left_menu_item_mailmagazine" href="../mailmagazine/index.html"><span class="hide_text">メールマガジン</span></a>
								<a id="column_left_menu_item_present" href="../present/index.html"><span class="hide_text">小冊子プレゼント</span></a>
								<a id="column_left_menu_item_qa" href="../qa/index.html"><span class="hide_text">Q&A</span></a>
							</div><!-- /div#column_left_menu -->
							<div id="column_left_company">
								<div id="column_left_company_logo"><span class="hide_text">株式会社進和不動産</span></div><!-- /div#column_left_company_logo -->
								<p id="column_left_company_address">〒591-8032<br />大阪府堺市北区百舌鳥梅町１丁30-1</p>
								<div id="column_left_company_phone"><span class="hide_text">電話 072-252-1049</span></div><!-- /div#column_left_company_phone -->
							</div><!-- /div#column_left_company -->
							<a id="column_left_blog_btn" href="../blog_staff/"><span class="hide_text">スタッフブログ</span></a>
						</div><!-- /div#column_left -->
						<div id="column_right" class="clearfix">
							<h2 id="h2_blog_staff"><span class="hide_text">スタッフブログ</span></h2>
							<div id="area_blog_staff" class="clearfix">
