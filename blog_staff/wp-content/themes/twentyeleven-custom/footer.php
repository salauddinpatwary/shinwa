							</div><!-- /div#area_blog_staff -->
						</div><!-- /div#column_right -->
					</div><!-- /div#board -->
					
				</div><!-- /div#pagecontent_wrap -->
			</div><!-- /div#pagecontent -->
		</div><!-- /div#stage -->
		<div id="footer">
			<div id="footer_frame">
				<div id="footer_wrap" class="clearfix">
					<p id="footer_menu"><a href="../privacy/index.html">プライバシーポリシー</a>　|　<a href="../link/index.html">関連リンク</a>　|　<a href="../sitemap/index.html">サイトマップ</a></p>
					<p id="footer_copyright">Copyright(c) 2012 Shinwa Real Estate Corporation. All Rights Reserved.</p>
				</div><!-- /div#footer_wrap -->
			</div><!-- /div#footer -->
		</div><!-- /div#footer -->
	</div><!-- /div#wrap -->
	<script type="text/javascript" src="/common/js/shinwa-f.js"></script>
</body>

</html>
