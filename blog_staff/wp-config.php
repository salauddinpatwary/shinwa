<?php
/**
 * The base configurations of the WordPress.
 *
 * このファイルは、MySQL、テーブル接頭辞、秘密鍵、言語、ABSPATH の設定を含みます。
 * より詳しい情報は {@link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86 
 * wp-config.php の編集} を参照してください。MySQL の設定情報はホスティング先より入手できます。
 *
 * このファイルはインストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さず、このファイルを "wp-config.php" という名前でコピーして直接編集し値を
 * 入力してもかまいません。
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - こちらの情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'shinwa');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'shinwa');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'password');

/** MySQL のホスト名 */
define('DB_HOST', '127.0.0.1:3306');

/** データベースのテーブルを作成する際のデータベースのキャラクターセット */
define('DB_CHARSET', 'utf8');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8{3,3;|6d#rA:BO-*uk|0m-j1.-X*%`|Fe(/>GvRqW~CVc($m$Iv0F:2g3,s9Gu~');
define('SECURE_AUTH_KEY',  '{B;bE2DTEx~%<3QHFMX)7|^|aB96z}W9dGe/iO`Tpp^8cAQ|%Ify0oSSy-08ef}9');
define('LOGGED_IN_KEY',    'fTHtG.ve9kSnEbiRo]%f`~)j/SX-_z^!^X50.+])J8Jmqnrdq(k3x?K7/i?Tj4u6');
define('NONCE_KEY',        '&gNE6b`(1?3cg|g)DCwf$`a|*q` 7Y:Ta=<Iczi%m8r)@s1%x{.JC-gPn|h3-+2H');
define('AUTH_SALT',        'wT,<++=_+Vv^-6sZ-<6{(fNi~x6HM.EO,@#D4@3-6xE$n %T,aU{=%+jiub=NZyM');
define('SECURE_AUTH_SALT', '-@3:6#$PSBnp5eLR+milXkqh6$<+mdaISHsJzvW>f&XwGN+N8DalvD~xb`+nbQT]');
define('LOGGED_IN_SALT',   'nJX?MQY4-=Wl1xYN1<%M^[kpCVzxYrt`Ql41JG7zt=^4a-Y9xj?n]c1+?DcH=N#_');
define('NONCE_SALT',       'h1_XM-i#t[si}R9#v?3:DW;d|LNt?=wCHK9v%)wE]M?q~n_t30sF|NF00_h6EW4%');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp0b8baf';

/**
 * ローカル言語 - このパッケージでは初期値として 'ja' (日本語 UTF-8) が設定されています。
 *
 * WordPress のローカル言語を設定します。設定した言語に対応する MO ファイルが
 * wp-content/languages にインストールされている必要があります。例えば de_DE.mo を
 * wp-content/languages にインストールし WPLANG を 'de_DE' に設定することでドイツ語がサポートされます。
 */
define('WPLANG', 'ja');

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 */
define('WP_DEBUG', 0);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
