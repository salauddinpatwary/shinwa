<?php header('Content-Type: text/html; charset='.get_option('blog_charset'), true); ?>
<?php /*
Template Name: ブログ-INDEX
*/ ?>

<?php get_header(); ?>

		<div id="container">
			<div id="content" role="main">

			<?php
			/* Run the loop to output the posts.
			 * If you want to overload this in a child theme then include a file
			 * called loop-index.php and that will be used instead.
			 */
			 get_template_part( 'loop', 'index' );
			?>
			</div><!-- #content -->
		</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>

<ul>

<?php if ( ! dynamic_sidebar( 'secondary-widget-area' ) ) : ?>

<?php endif; // end secondary widget area ?>

</ul>
