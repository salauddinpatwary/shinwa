=== fudoukaiin ===

Contributors: nendeb
Tags: fudousan,map,bukken,estate,fudou,GoogleMaps
Requires at least: 4.4
Tested up to: 4.7
Stable tag: 1.7.14

Fudousan kaiin Plugin for Real Estate

== Description ==

Fudousan kaiin Plugin for Real Estate

== Installation ==

Installing the plugin:
1. Download the zipped plugin file to your local machine.
2. Unzip the file.
3. Upload the `fudoukaiin` folder to the `/wp-content/plugins/` directory.
4. Activate the plugin through the 'Plugins' menu in WordPress.

== Requirements ==

* WordPress 4.4 or later
* PHP 5.4 or later (NOT support PHP 4.x!!)

== Credits ==

This plugin uses  Fudousan Plugin

== Upgrade Notice ==

The required WordPress version has been changed and now requires WordPress 4.4 or higher

== Frequently Asked Questions ==

Use these support channels appropriately.

1. [Docs](http://nendeb.jp/)

== Changelog ==

= 1.7.14 =
* Fixed wp-login.php
* Fixed fudoukaiin.php

= 1.7.12 =
* Fixed wp-login.php
* Fixed fudo-widget_kaiin.php
* Fixed wp-login.php

= 1.7.9 =
* Fixed wp-login.php
* Fixed fudoukaiin.php
* Fixed login.css
* Fixed utils.min.js

= 1.7.8 =
* Fixed fudo-widget_kaiin.php

= 1.7.7 =
* Fixed wp-login.php
* Add Return-Path.
* Fixed admin_fudoukaiin.php
* Fixed fudoukaiin.php

= 1.7.4 =
* Fixed kaiin_register to mobile.

= 1.7.2 =
* Fixed wp-login.php

= 1.7.0 =
* Fixed php7

= 1.6.5 =
* Fixed WordPress4.3

= v1.5.3 =
* Fixed Permalinks Check.

= v1.5.0 =
* Fixed WordPress4.0.

= v1.4.5 =
* Fixed WordPress3.9a

= v1.4.1=
* Fixed WordPress3.7

= v1.4.0=
* Fixed WordPress3.7

= v1.1.7 =
* Fixed WordPress3.6

= v1.1.6 =
* Fixed administrators login

= v1.1.5 =
* Fixed show_admin_bar_front false

= v1.1.4 =
* Fixed re password

= v1.1.3 =
* Fixed WordPress3.5

= v1.1.2 =
* Fixed: other Items.

= v1.1.0 =
* Fixed: fudoukaiin.php.
* Fixed: wp-login.php.

= v1.0.6 =
* Fixed: SSL Items.

= v1.0.5 =
* Fixed: other Items.

= v1.0.1 =
* Fixed: for WordPress3.3.

= v1.0.0 =
* Initial version of the plugin

