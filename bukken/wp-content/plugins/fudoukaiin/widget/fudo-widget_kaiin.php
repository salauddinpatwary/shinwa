<?php
/*
 * 不動産会員プラグインウィジェット
 * @package WordPress4.7
 * @subpackage Fudousan Plugin
 * Version: 1.7.12
*/



// 会員ログインウィジェット
function fudo_widgetInit_kaiin() {
	if( defined( 'FUDOU_VERSION' ) ){
		register_widget('fudo_widget_kaiin');
	}
}
add_action('widgets_init', 'fudo_widgetInit_kaiin');


// 会員ログインウィジェット
class fudo_widget_kaiin extends WP_Widget {

	/**
	 * Register widget with WordPress 4.3.
	 */
	function __construct() {
		parent::__construct(
			'fudo_kaiin', 			// Base ID
			'会員ログイン' ,		// Name
			array( 'description' => '', )	// Args
		);
	}

	/** @see WP_Widget::form */	
	function form($instance) {
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$caution = isset($instance['caution']) ?  $instance['caution'] : '';
		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>">
		<?php _e('title'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>

		<p><label for="<?php echo $this->get_field_id('caution'); ?>">
		注意事項 <textarea rows='5' cols='15' class="widefat" id="<?php echo $this->get_field_id('caution'); ?>" name="<?php echo $this->get_field_name('caution'); ?>"><?php echo $caution; ?></textarea></label></p>

		<?php 
	}

	/** @see WP_Widget::update */
	function update($new_instance, $old_instance) {
		return $new_instance;
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance) {

		// outputs the content of the widget
		extract( $args );
		$title = '';
		$caution = '';
		$title = apply_filters('widget_title', $instance['title']);
		$caution = isset($instance['caution']) ? $instance['caution'] : '';

		if( is_home() ) {
			$redirect_to = get_bloginfo('url');
		}else{
		//	$redirect_to = get_permalink();
			$redirect_to = esc_url( $_SERVER['REQUEST_URI'] );
		}


	if( get_option('kaiin_users_can_register' ) ){

		global $user_ID;
		global $user_login;
		global $chenged_user_id;

		$user = new WP_User( $user_ID );
		if( isset( $user->roles[0] ) ){
			$user_roles = $user->roles[0];	//subscriber
		}else{
			$user_roles = '';
		}

		echo $before_widget;

		//SSL
		$fudou_ssl_site_url = get_option('fudou_ssl_site_url');
		if( $fudou_ssl_site_url !=''){
			$site_url = $fudou_ssl_site_url;
		}else{
			$site_url = get_option('siteurl');
		}


		if ( $title != '' )
			echo $before_title . $title . $after_title; 

			if ( !is_user_logged_in() ){

				$rememberme = ! empty( $_POST['rememberme'] );

				?>
				<?php echo $caution; ?>

				<div class="kaiin_login">
					<?php if( isset($_POST['md']) && $_POST['md'] == 'login' ) echo "<p>ログイン失敗しました。</p>"; ?>

					<form  name="loginform" id="loginform" method="post">
					<label for="user_login">ユーザー名</label>
					<input type="text" name="log" id="user_login" class="input" value="" size="10" tabindex="10" /><br />
					<label for="password">パスワード</label>
					<input type="password" name="pwd" id="password" class="input" value="" size="10" tabindex="20" /><br />
					<input type="text" name="mail" value="" size="10" style="display:none;" />

					<?php //do_action( 'login_form' ); ?>

					<input type="checkbox" name="rememberme" id="rememberme" value="forever" tabindex="90"<?php checked( $rememberme ); ?> /> ログイン情報を記憶<br />
					<input type="hidden" name="redirect_to" value="<?php echo $redirect_to;?>" />
					<input type="hidden" name="testcookie" value="1" />
					<input type="hidden" name="md" value="login" />
					<input type="submit" name="submit" value="ログイン" />
					<input type="hidden" name="fudou_userlogin_nonce" value="<?php echo wp_create_nonce( 'fudou_userlogin_nonce' ); ?>" />

					</form>

					<div class="kaiin_register">
						<?php if ( wp_is_mobile() ) { ?>
							<?php if( get_option('kaiin_moushikomi') != 1 ){ ?>
							<a href="<?php echo $site_url; ?>/wp-content/plugins/fudoukaiin/wp-login.php?action=register">会員登録</a> | 
							<?php } ?>
							<a href="<?php echo $site_url; ?>/wp-content/plugins/fudoukaiin/wp-login.php?action=lostpassword">パスワード忘れた</a>
						<?php }else{ ?>
							<?php if( get_option('kaiin_moushikomi') != 1 ){ ?>
							<a href="<?php echo $site_url; ?>/wp-content/plugins/fudoukaiin/wp-login.php?action=register&amp;KeepThis=true&amp;TB_iframe=true&amp;height=500&amp;width=400" class="thickbox">会員登録</a> | 
							<?php } ?>
							<a href="<?php echo $site_url; ?>/wp-content/plugins/fudoukaiin/wp-login.php?action=lostpassword&amp;KeepThis=true&amp;TB_iframe=true&amp;height=350&amp;width=400" class="thickbox">パスワード忘れた</a>
						<?php } ?>
					</div>

				</div>
				<?php
				//gianism
				if (function_exists('gianism_login')){
					echo '<div class="gianism_login">';
					echo "こちらからでもログインできます。";
					gianism_login();
					echo '</div>';
				}


			} else { 

				echo '<div class="login_success">';

					$repass_md = isset($_POST['md']) ? esc_attr($_POST['md']) : '';
					$new_pass  = isset($_POST['pass1']) ? trim($_POST['pass1']) : '';
					$new_pass2 = isset($_POST['pass2']) ? trim($_POST['pass2']) : '';
					$repass_user_nonce = isset($_POST['fudou_repass_user_nonce']) ?  $_POST['fudou_repass_user_nonce'] : '';	//verify_nonce

					//verify_nonce
					if( $repass_user_nonce && wp_verify_nonce( $repass_user_nonce, 'fudou_repass_user_nonce') ) {

						if ( $user_ID && $new_pass != '' && $new_pass2 != '' && $repass_md == 'repass' ) {
							if ($new_pass == $new_pass2 ){

								if ( $chenged_user_id == $user_ID ) {
									echo '<p class="message">パスワードを変更しました。</p>';
								}else{
									echo '<p class="login_error">パスワード変更に失敗しました。</p>';
								}

							} else {
								echo '<p class="login_error">パスワードが合いません。</p>';
							}
						}
					}
					//簡易ログ
					fudoukaiin_userlogin_success($user_ID);

?>
					<span class="login_comment">こんにちは！<?php echo $user_login; ?>さん</span>
					<?php echo apply_filters( 'fudo_kaiin_login_success', '' ); ;?>
					<br />

					<?php if( $user_roles == 'subscriber' ){  //subscriber限定 ?>
						<span class="logout_title"><a href="<?php echo get_bloginfo("url") ."/?md=logout";?>">ログアウト</a></span> | <span class="repass_title"><a href="javascript:repass('repass');">パスワード変更</a></span>
					<?php } ?>
				</div>


				<div id="repass">
				<br />

					<form  id="rpass" name="form" method="post" action="">
					<input type="hidden" id="user_login" value="<?php echo $user_login; ?>" autocomplete="off" />
					<input type="hidden" name="user_id" value="<?php echo $user_ID; ?>" autocomplete="off" />

					<label for="pass1">新しいパスワードを入力して下さい</label>
					<input type="text" name="pass1" id="pass1" class="input" value="" size="10" tabindex="20" autocomplete="off" />

					<label for="pass2">もう一度入力して下さい</label>
					<input type="text" name="pass2" id="pass2" class="input" value="" size="10" tabindex="20" autocomplete="off" />

					<div id="pass-strength-result" class="hide-if-no-js"><?php _e('Strength indicator'); ?></div>
					<input type="submit" name="submit" value="パスワード変更" />

					<p class="description indicator-hint"><?php echo wp_get_password_hint(); ?></p>

					<input type="hidden" name="md" value="repass" />
					<input type="hidden" name="fudou_repass_user_nonce" value="<?php echo wp_create_nonce( 'fudou_repass_user_nonce' ); ?>" />
					</form>
				</div>

				<?php

			global $is_fudouktai,$is_fudoumap,$is_fudoukaiin,$is_fudoumail;

			//マッチングメール設定 (subscriber限定)
			if( get_option('kaiin_users_mail') =='1' && $is_fudoumail &&  $user_roles == 'subscriber' ){
				echo '<ul>';
				echo '<div id="maching_mail">';

				if ( wp_is_mobile() ) { 
					echo '<a href="'.WP_PLUGIN_URL.'/fudoumail/fudou_user.php">';
				}else{
					echo '<a href="'.WP_PLUGIN_URL.'/fudoumail/fudou_user.php?KeepThis=true&TB_iframe=true&height=90%&width=680" class="thickbox">';
				}

				if( get_option('kaiin_users_rains_register') =='1' && $is_fudoumail ){
					echo '閲覧条件・メール設定</a></div>';
				}else{
					echo 'マッチングメール設定</a></div>';
				}
				echo '</ul>';
			}

			?>

				<style type="text/css" media="all">
				<!--
					input#pass1,
					input#pass2{
						width: 98% !important;
						display: block;
					}

					#repass { display:none; }
					ul #repass { display:none; }

					#repass .indicator-hint{
						margin: 10px 0;
					}


					#pass-strength-result {
						background-color: #eee;
						border-color: #ddd !important;
						border-style: solid;
						border-width: 1px;
						margin:5px 0;
						padding: 5px;
						text-align: center;
						width: auto;
						display: none;
					}

					#pass-strength-result.bad {
						background-color: #ffb78c;
						border-color: #ff853c !important;
					}

					#pass-strength-result.good {
						background-color: #ffec8b;
						border-color: #ffcc00 !important;
					}

					#pass-strength-result.short {
						background-color: #ffa0a0;
						border-color: #f04040 !important;
					}

					#pass-strength-result.strong {
						background-color: #c3ff88;
						border-color: #8dff1c !important;
					}

					#main #maching_mail a {
						background: #ce6166 none repeat scroll 0 0;
						border-radius: 2px;
						color: #ffffff;
						display:block;

						font-size: 14px;
						font-weight: 700;
						margin: 5px 0px;
						padding: 8px;
						text-align: center;
						text-decoration: none;
						vertical-align: middle;
						max-width: 20em;
					}

					#main #maching_mail a:hover {
						background: #ff9b9d;
						text-decoration: underline;
					}

					#main #maching_mail a:active {
						background: #faa8cd;
						text-decoration: none;
					}
				-->
				</style>

				<!-- .3.6.1 -->
				<script type="text/javascript" src="<?php bloginfo('wpurl'); ?>/wp-content/plugins/fudoukaiin/js/password-strength-meter.min.js"></script>
				<script type="text/javascript" src="<?php bloginfo('wpurl'); ?>/wp-content/plugins/fudoukaiin/js/user-profile.min.js"></script>

				<script type='text/javascript'>
					function repass(menu_id) {
						var ul=document.getElementById(menu_id);
						if (ul.style.display == "block") ul.style.display="none";
						else {
							ul.style.display="block";
						};
					};
					var pwsL10n = {
					 empty: "強度インジケータ",
					 short: "非常に弱い",
					 bad: "弱い",
					 good: "普通",
					 strong: "強力",
					 mismatch: "不一致"
					};
					try{convertEntities(pwsL10n);}catch(e){};
				</script>

			<?php 
			}	//is_user_logged_in()

			echo $after_widget;
		}
	}
} // fudo_widget_kaiin




// 物件カウント表示
function fudo_widgetInit_bukkensu() {
	if( defined( 'FUDOU_VERSION' ) ){
		register_widget('fudo_widget_bukkensu');
	}
}
add_action('widgets_init', 'fudo_widgetInit_bukkensu');

// 物件カウント表示ウィジェット
class fudo_widget_bukkensu extends WP_Widget {

	/**
	 * Register widget with WordPress 4.3.
	 */
	function __construct() {
		parent::__construct(
			'fudo_bukkensu', 		// Base ID
			'物件カウント表示' ,		// Name
			array( 'description' => '', )	// Args
		);
	}

	/** @see WP_Widget::form */	
	function form($instance) {

		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
	//	$caution = isset($instance['caution']) ?  $instance['caution'] : '';
		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>">
		<?php _e('title'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>

		<?php 
	}

	/** @see WP_Widget::update */
	function update($new_instance, $old_instance) {
		return $new_instance;
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance) {

		global $wpdb;
		$num_ippan = 0;
		$num_kaiin = 0;

		// outputs the content of the widget
	        extract( $args );

		$title = '';
	        $title = apply_filters('widget_title', $instance['title']);

		echo "\n";
		echo $before_widget;

		if ( $title != '' )
			echo $before_title . $title . $after_title; 

		//物件カウント

		//公開
		$sql = "SELECT count(DISTINCT P.ID) AS co";
		$sql .=  " FROM $wpdb->posts AS P";
		$sql .=  " INNER JOIN $wpdb->postmeta AS PM ON P.ID = PM.post_id ";
		$sql .=  " WHERE P.post_status='publish' AND P.post_password = ''  AND P.post_type ='fudo' ";
		$sql .=  " AND PM.meta_key='kaiin' AND PM.meta_value = 0";
	//	$sql = $wpdb->prepare($sql,'');
		$metas = $wpdb->get_row( $sql );
		if( !empty($metas) ){
			$num_ippan = $metas->co;
		}


		//会員
		$sql = "SELECT count(DISTINCT P.ID) AS co";
		$sql .=  " FROM $wpdb->posts AS P";
		$sql .=  " INNER JOIN $wpdb->postmeta AS PM ON P.ID = PM.post_id ";
		$sql .=  " WHERE P.post_status='publish' AND P.post_password = ''  AND P.post_type ='fudo' ";
		$sql .=  " AND PM.meta_key='kaiin' AND PM.meta_value = 1";
		//$sql = $wpdb->prepare($sql,'');
		$metas = $wpdb->get_row( $sql );
		if( !empty($metas) ){
			$num_kaiin = $metas->co;
		}


		echo '<div class="kaiin_count">';
		echo '<ul>';
		echo '<li>一般公開物件：<span style="color:#FF0000;">'.$num_ippan.'件</span></li>';
		echo '<li>会員限定物件：<span style="color:#FF0000;">'.$num_kaiin.'件</span></li>';

		do_action( 'kaiin_level_bukken_count' );

		echo '</ul>';
		echo '</div>';

		echo $after_widget;
		echo "\n";

	}
} // fudo_widget_kaiin

