<?php
/**
 * WordPress User Page
 *
 * Handles authentication, registering, resetting passwords, forgot password,
 * and other user handling.
 *
 * @package WordPress4.7
 * @subpackage Fudousan Plugin
 * Fudousan kaiin Plugin
 * Version: 1.7.13
 */

/** Make sure that the WordPress bootstrap has run before continuing. */
require( dirname(__FILE__) . '/../../../wp-load.php' );

remove_action('wp_print_styles', 'jqlb_css');	
remove_action('wp_print_scripts', 'jqlb_js');


// Redirect to https login if forced to use SSL
if ( force_ssl_admin() && ! is_ssl() ) {
	if ( 0 === strpos($_SERVER['REQUEST_URI'], 'http') ) {
		wp_redirect( set_url_scheme( $_SERVER['REQUEST_URI'], 'https' ) );
		exit();
	} else {
		wp_redirect( 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );
		exit();
	}
}

/**
 * Output the login page header.
 *
 * @param string   $title    Optional. WordPress login Page title to display in the `<title>` element.
 *                           Default 'Log In'.
 * @param string   $message  Optional. Message to display in header. Default empty.
 * @param WP_Error $wp_error Optional. The error to pass. Default empty.
 */
function fudoukaiin_login_header( $title = 'Log In', $message = '', $wp_error = '' ) {
	global $error, $interim_login, $action;

	//SSL
	$fudou_ssl_site_url = get_option('fudou_ssl_site_url');
	if( $fudou_ssl_site_url !=''){
		$site_url = $fudou_ssl_site_url;
	}else{
		$site_url = get_option('siteurl');
	}

	// Don't index any of these forms
	add_filter( 'pre_option_blog_public', '__return_zero' );
	add_action( 'login_head', 'noindex' );

	if ( empty($wp_error) )
		$wp_error = new WP_Error();

	// Shake it!
	$shake_error_codes = array( 'empty_password', 'empty_email', 'invalid_email', 'invalidcombo', 'empty_username', 'invalid_username', 'incorrect_password' );
	/**
	 * Filters the error codes array for shaking the login form.
	 *
	 * @since 3.0.0
	 *
	 * @param array $shake_error_codes Error codes that shake the login form.
	 */
	$shake_error_codes = apply_filters( 'shake_error_codes', $shake_error_codes );

	$separator = is_rtl() ? ' &rsaquo; ' : ' &lsaquo; ';

	?><!DOCTYPE html>
	<!--[if IE 8]>
		<html xmlns="http://www.w3.org/1999/xhtml" class="ie8" <?php language_attributes(); ?>>
	<![endif]-->
	<!--[if !(IE 8) ]><!-->
		<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
	<!--<![endif]-->
	<head>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<title><?php echo get_bloginfo( 'name', 'display' ) . $separator . $title; ?></title>

	<link rel="stylesheet" id="login-css"  href="<?php echo $site_url; ?>/wp-content/plugins/fudoukaiin/login.css" type="text/css" media="all" />
	<script type="text/javascript" src="<?php echo $site_url; ?>/wp-includes/js/jquery/jquery.js"></script>
	<script type="text/javascript" src="<?php echo $site_url; ?>/wp-content/plugins/fudoukaiin/js/utils.min.js?ver=4.4"></script>

	<meta name='robots' content='noindex,nofollow' />

	<?php	if ( wp_is_mobile() ) { ?>
		<meta name="viewport" content="width=320; initial-scale=0.9; maximum-scale=1.0; user-scalable=0;" />
		<style type="text/css" media="screen">
			form { margin-left: 0px; }
			#login { margin-top: 20px; }
		</style>
	<?php	} elseif ( isset($interim_login) && $interim_login ) { 	?>
		<style type="text/css" media="all">
			body {padding-top: 0px;}
			.login #login { margin: 0px auto; }
		</style>
	<?php	}

		if ( $shake_error_codes && $wp_error->get_error_code() && in_array( $wp_error->get_error_code(), $shake_error_codes ) )
			fudoukaiin_wp_shake_js();
		?>
	</head>
<body class="login wp-core-ui">
	<?php
	/**
	 * Fires in the login page header after the body tag is opened.
	 *
	 * @since 4.6.0
	 */
	do_action( 'login_header' );
	?>
	<div id="login">

	<?php
	/**
	 * Filters the message to display above the login form.
	 *
	 * @since 2.1.0
	 *
	 * @param string $message Login message text.
	 */
	$message = apply_filters( 'login_message', $message );
	if ( !empty( $message ) )
		echo $message . "\n";

	// In case a plugin uses $error rather than the $wp_errors object
	if ( !empty( $error ) ) {
		$wp_error->add('error', $error);
		unset($error);
	}

	if ( $wp_error->get_error_code() ) {
		$errors = '';
		$messages = '';
		foreach ( $wp_error->get_error_codes() as $code ) {
			$severity = $wp_error->get_error_data( $code );
			foreach ( $wp_error->get_error_messages( $code ) as $error_message ) {
				if ( 'message' == $severity )
					$messages .= '	' . $error_message . "<br />\n";
				else
					$errors .= '	' . $error_message . "<br />\n";
			}
		}
		if ( ! empty( $errors ) ) {
			/**
			 * Filters the error messages displayed above the login form.
			 *
			 * @since 2.1.0
			 *
			 * @param string $errors Login error message.
			 */
			$errors = str_replace( 'wp-login.php' , 'wp-content/plugins/fudoukaiin/wp-login.php' , $errors );
			echo '<div id="login_error">' . apply_filters( 'login_errors', $errors ) . "</div>\n";
		}
		if ( ! empty( $messages ) ) {
			/**
			 * Filters instructional messages displayed above the login form.
			 *
			 * @since 2.5.0
			 *
			 * @param string $messages Login messages.
			 */
			echo '<p class="message">' . apply_filters( 'login_messages', $messages ) . "</p>\n";
		}
	}
} // End of fudoukaiin_login_header()




/**
 * Outputs the footer for the login page.
 *
 * @param string $input_id Which input to auto-focus
 */
function fudoukaiin_login_footer($input_id = '') {
	global $interim_login;

	//SSL
	$fudou_ssl_site_url = get_option('fudou_ssl_site_url');
	if( $fudou_ssl_site_url !=''){
		$site_url = $fudou_ssl_site_url;
	}else{
		$site_url = get_option('siteurl');
	}
	$site_url_localize = str_replace( '/', '\/', $site_url );

	?>
	</div>

	<?php if ( !empty($input_id) ) : ?>
	<script type="text/javascript">
	try{document.getElementById('<?php echo $input_id; ?>').focus();}catch(e){}
	if(typeof wpOnload=='function')wpOnload();
	</script>
	<?php endif; ?>

	<?php
	/**
	 * Fires in the login page footer.
	 *
	 * @since 3.1.0
	 */
	//do_action( 'login_footer' ); 
	?>
	<script type='text/javascript'>
	/* <![CDATA[ */
	var thickboxL10n = {"next":"\u6b21\u3078 >","prev":"< \u524d\u3078","image":"\u753b\u50cf","of":"\/","close":"\u9589\u3058\u308b","noiframes":"\u3053\u306e\u6a5f\u80fd\u3067\u306f iframe \u304c\u5fc5\u8981\u3067\u3059\u3002\u73fe\u5728 iframe \u3092\u7121\u52b9\u5316\u3057\u3066\u3044\u308b\u304b\u3001\u5bfe\u5fdc\u3057\u3066\u3044\u306a\u3044\u30d6\u30e9\u30a6\u30b6\u30fc\u3092\u4f7f\u3063\u3066\u3044\u308b\u3088\u3046\u3067\u3059\u3002","loadingAnimation":"<?php echo $site_url_localize;?>\/wp-includes\/js\/thickbox\/loadingAnimation.gif"};
	var userSettings = {"url":"<?php echo $site_url_localize;?>\/","uid":"0","time":"1480905648","secure":""};
	var _zxcvbnSettings = {"src":"<?php echo $site_url_localize;?>\/wp-includes\/js\/zxcvbn.min.js"};
	var pwsL10n = {"unknown":"\u30d1\u30b9\u30ef\u30fc\u30c9\u5f37\u5ea6\u4e0d\u660e","short":"\u975e\u5e38\u306b\u8106\u5f31","bad":"\u8106\u5f31","good":"\u666e\u901a","strong":"\u5f37\u529b","mismatch":"\u4e0d\u4e00\u81f4"};
	var _wpUtilSettings = {"ajax":{"url":"<?php echo $site_url_localize;?>\/wp-admin\/admin-ajax.php"}};
	var userProfileL10n = {"warn":"\u65b0\u3057\u3044\u30d1\u30b9\u30ef\u30fc\u30c9\u306f\u3001\u4fdd\u5b58\u3055\u308c\u3066\u3044\u307e\u305b\u3093\u3002","warnWeak":"\u8106\u5f31\u306a\u30d1\u30b9\u30ef\u30fc\u30c9\u306e\u4f7f\u7528\u3092\u78ba\u8a8d","show":"\u8868\u793a\u3059\u308b","hide":"\u96a0\u3059","cancel":"\u30ad\u30e3\u30f3\u30bb\u30eb","ariaShow":"\u30d1\u30b9\u30ef\u30fc\u30c9\u3092\u8868\u793a","ariaHide":"\u30d1\u30b9\u30ef\u30fc\u30c9\u3092\u96a0\u3059"};
	/* ]]> */
	</script>
	<script type='text/javascript' src='<?php echo $site_url; ?>/wp-admin/load-scripts.php?c=0&amp;load%5B%5D=jquery-core,jquery-migrate,thickbox,utils,zxcvbn-async,password-strength-meter,underscore,wp-util,user-profile&amp;ver='></script>

	<div class="clear"></div>
	</body>
	</html>
	<?php
}

/**
 * @since 3.0.0
 */
function fudoukaiin_wp_shake_js() {
	if ( wp_is_mobile() )
		return;
?>
<script type="text/javascript">
addLoadEvent = function(func){if(typeof jQuery!="undefined")jQuery(document).ready(func);else if(typeof wpOnload!='function'){wpOnload=func;}else{var oldonload=wpOnload;wpOnload=function(){oldonload();func();}}};
function s(id,pos){g(id).left=pos+'px';}
function g(id){return document.getElementById(id).style;}
function shake(id,a,d){c=a.shift();s(id,c);if(a.length>0){setTimeout(function(){shake(id,a,d);},d);}else{try{g(id).position='static';wp_attempt_focus();}catch(e){}}}
addLoadEvent(function(){ var p=new Array(15,30,15,0,-15,-30,-15,0);p=p.concat(p.concat(p));var i=document.forms[0].id;g(i).position='relative';shake(i,p,20);});
</script>
<?php
}




/**
 * Handles sending password retrieval email to user.
 *
 * @global wpdb         $wpdb      WordPress database abstraction object.
 * @global PasswordHash $wp_hasher Portable PHP password hashing framework.
 *
 * @return bool|WP_Error True: when finish. WP_Error on error
 */
function fudoukaiin_retrieve_password() {

	global $wpdb, $wp_hasher;

	//差出人変更
	$fudoukaiin_wp_mail_from = new fudoukaiin_wp_mail_from();

	//SSL
	$fudou_ssl_site_url = get_option('fudou_ssl_site_url');
	if( $fudou_ssl_site_url !=''){
		$site_url = $fudou_ssl_site_url;
	}else{
		$site_url = get_option('siteurl');
	}

	$errors = new WP_Error();

	$u_login = isset( $_POST['user_login'] ) ? trim( $_POST['user_login'] ) : '';
	$u_email = isset( $_POST['user_email'] ) ? trim( $_POST['user_email'] ) : '';

	if ( empty( $u_login ) && empty( $_POST['user_email'] ) )
		$errors->add('empty_username', __('<strong>ERROR</strong>: Enter a username or email address.'));

	//スパム対策
	if (  $u_login == 'admin'  )
		$errors->add('empty_username', __('<strong>ERROR</strong>: Enter a username or email address.'));
	if (  isset($_POST['mail']) && $_POST['mail'] != ''  )
		$errors->add('empty_username', __('<strong>ERROR</strong>: Enter a username or email address.'));


	if ( isset( $u_login ) && strpos( $u_login, '@' ) ) {
		$user_data = get_user_by( 'email', wp_unslash( $u_login ) );
		if ( empty( $user_data ) )
			$errors->add('invalid_email', __('<strong>ERROR</strong>: There is no user registered with that email address.'));
	} else {
		$login = $u_login;
		$user_data = get_user_by('login', $login);
	}

	/**
	 * Fires before errors are returned from a password reset request.
	 *
	 * @since 2.1.0
	 * @since 4.4.0 Added the `$errors` parameter.
	 *
	 * @param WP_Error $errors A WP_Error object containing any errors generated
	 *                         by using invalid credentials.
	 */
	do_action( 'lostpassword_post', $errors );

	if ( $errors->get_error_code() )
		return $errors;

	if ( !$user_data ) {
		$errors->add('invalidcombo', __('<strong>ERROR</strong>: Invalid username or email.'));
		return $errors;
	}

	// Redefining user_login ensures we return the right case in the email.
	$user_login = $user_data->user_login;
	$user_email = $user_data->user_email;
	$key = get_password_reset_key( $user_data );

	if ( is_wp_error( $key ) ) {
		return $key;
	}

	$message = __('Someone has requested a password reset for the following account:') . "\r\n\r\n";
	$message .= network_home_url( '/' ) . "\r\n\r\n";
	$message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
	$message .= __('If this was a mistake, just ignore this email and nothing will happen.') . "\r\n\r\n";
	$message .= __('To reset your password, visit the following address:') . "\r\n\r\n";
	//$message .= '<' . network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login), 'login') . ">\r\n";
	$message .= '' . $site_url . "/wp-content/plugins/fudoukaiin/wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login) . "\r\n";

	if ( is_multisite() )
		$blogname = $GLOBALS['current_site']->site_name;
	else
		/*
		 * The blogname option is escaped with esc_html on the way into the database
		 * in sanitize_option we want to reverse this for the plain text arena of emails.
		 */
		$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

	$title = sprintf( __('[%s] Password Reset'), $blogname );

	/**
	 * Filters the subject of the password reset email.
	 *
	 * @since 2.8.0
	 * @since 4.4.0 Added the `$user_login` and `$user_data` parameters.
	 *
	 * @param string  $title      Default email title.
	 * @param string  $user_login The username for the user.
	 * @param WP_User $user_data  WP_User object.
	 */
	$title = apply_filters( 'retrieve_password_title', $title, $user_login, $user_data );

	/**
	 * Filters the message body of the password reset mail.
	 *
	 * @since 2.8.0
	 * @since 4.1.0 Added `$user_login` and `$user_data` parameters.
	 *
	 * @param string  $message    Default mail message.
	 * @param string  $key        The activation key.
	 * @param string  $user_login The username for the user.
	 * @param WP_User $user_data  WP_User object.
	 */
	$message = apply_filters( 'retrieve_password_message', $message, $key, $user_login, $user_data );

	if ( $message && !wp_mail( $user_email, wp_specialchars_decode( $title ), $message ) )
		wp_die( __('The email could not be sent.') . "<br />\n" . __('Possible reason: your host may have disabled the mail() function.') );

	return true;
}










/**
 * Retrieves a user row based on password reset key and login
 *
 * @uses $wpdb WordPress Database object
 *
 * @param string $key Hash to validate sending user's password
 * @param string $login The user login
 *
 * @return object|WP_Error
 */
function check_password_reset_key_fudou($key, $login) {

	global $wpdb;

	$key = preg_replace('/[^a-z0-9]/i', '', $key);

	if ( empty( $key ) || !is_string( $key ) )
		return new WP_Error('invalid_key', __('Invalid key'));

	if ( empty($login) || !is_string($login) )
		return new WP_Error('invalid_key', __('Invalid key'));

	$user = $wpdb->get_row($wpdb->prepare("SELECT * FROM $wpdb->users WHERE user_activation_key = %s AND user_login = %s", $key, $login));

	if ( empty( $user ) )
		return new WP_Error('invalid_key', __('Invalid key'));

	return $user;
}



/**
 * Handles resetting the user's password.
 *
 * @uses $wpdb WordPress Database object
 *
 * @param string $key Hash to validate sending user's password
 */
function reset_password_fudou($user, $new_pass) {
	do_action('password_reset', $user, $new_pass);
	wp_set_password($new_pass, $user->ID);
	wp_password_change_notification($user);
}



/**
 * Handles registering a new user.
 *
 * @param string $user_login User's username for logging in
 * @param string $user_email User's email address to send password and add
 * @return int|WP_Error Either user's ID or error on failure.
 *
 * Fudousan kaiin Plugin
 * Version: 1.7.13
 */
function register_new_user_fudou( $user_login, $user_email ) {

	$errors = new WP_Error();

	//option
	$user_zip = isset($_POST['user_zip']) ? esc_attr($_POST['user_zip']) : '';
	$user_adr = isset($_POST['user_adr']) ? esc_attr($_POST['user_adr']) : '';
	$user_tel = isset($_POST['user_tel']) ? esc_attr($_POST['user_tel']) : '';
	$first_name = isset($_POST['first_name']) ? esc_attr($_POST['first_name']) : '';
	$last_name  = isset($_POST['last_name']) ? esc_attr($_POST['last_name']) : '';

	$sanitized_user_login = sanitize_user( $user_login );
	/**
	 * Filters the email address of a user being registered.
	 *
	 * @since 2.1.0
	 *
	 * @param string $user_email The email address of the new user.
	 */
	$user_email = apply_filters( 'user_registration_email', $user_email );

	// Check the username
	if ( $sanitized_user_login == '' ) {
		$errors->add( 'empty_username', __( '<strong>ERROR</strong>: Please enter a username.' ) );
	} elseif ( ! validate_username( $user_login ) ) {
		$errors->add( 'invalid_username', __( '<strong>ERROR</strong>: This username is invalid because it uses illegal characters. Please enter a valid username.' ) );
		$sanitized_user_login = '';
	} elseif ( username_exists( $sanitized_user_login ) ) {
		$errors->add( 'username_exists', __( '<strong>ERROR</strong>: This username is already registered. Please choose another one.' ) );

	} else {
		/** This filter is documented in wp-includes/user.php */
		$illegal_user_logins = array_map( 'strtolower', (array) apply_filters( 'illegal_user_logins', array() ) );
		if ( in_array( strtolower( $sanitized_user_login ), $illegal_user_logins ) ) {
			$errors->add( 'invalid_username', __( '<strong>ERROR</strong>: Sorry, that username is not allowed.' ) );
		}
	}

	// Check the email address
	if ( $user_email == '' ) {
		$errors->add( 'empty_email', __( '<strong>ERROR</strong>: Please type your email address.' ) );
	} elseif ( ! is_email( $user_email ) ) {
		$errors->add( 'invalid_email', __( '<strong>ERROR</strong>: The email address isn&#8217;t correct.' ) );
		$user_email = '';
	} elseif ( email_exists( $user_email ) ) {
		$errors->add( 'email_exists', __( '<strong>ERROR</strong>: This email is already registered, please choose another one.' ) );
	}


	// Check the Option
	if( get_option('kaiin_users_mail_name') == '1' ){
		if( $first_name =='' && $last_name == '' && get_option('kaiin_users_mail_name_hissu') == '1' )
			$errors->add( 'empty_username', '<strong>エラー</strong>: お名前を入力してください。' );
	}
	if( get_option('kaiin_users_mail_zip') == '1' ){
		if( $user_zip =='' && get_option('kaiin_users_mail_zip_hissu') == '1' )
			$errors->add( 'empty_username', '<strong>エラー</strong>: 郵便番号を入力してください。' );
	}
	if( get_option('kaiin_users_mail_address') == '1' ){
		if( $user_adr =='' && get_option('kaiin_users_mail_address_hissu') == '1' )
			$errors->add( 'empty_username', '<strong>エラー</strong>: 住所を入力してください。' );
	}

	if( get_option('kaiin_users_mail_tel') == '1' ){
		if( $user_tel =='' && get_option('kaiin_users_mail_tel_hissu') == '1' )
			$errors->add( 'empty_username', '<strong>エラー</strong>: 電話番号を入力してください。' );
	}
	if( mb_strlen( $sanitized_user_login) < 4 ){
			$errors->add( 'empty_username', '<strong>エラー</strong>: ユーザー名が短すぎです。' );
	}

	/**
	 * Fires when submitting registration form data, before the user is created.
	 *
	 * @since 2.1.0
	 *
	 * @param string   $sanitized_user_login The submitted username after being sanitized.
	 * @param string   $user_email           The submitted email.
	 * @param WP_Error $errors               Contains any errors with submitted username and email,
	 *                                       e.g., an empty field, an invalid username or email,
	 *                                       or an existing username or email.
	 */
	do_action( 'register_post', $sanitized_user_login, $user_email, $errors );

	/**
	 * Filters the errors encountered when a new user is being registered.
	 *
	 * The filtered WP_Error object may, for example, contain errors for an invalid
	 * or existing username or email address. A WP_Error object should always returned,
	 * but may or may not contain errors.
	 *
	 * If any errors are present in $errors, this will abort the user's registration.
	 *
	 * @since 2.1.0
	 *
	 * @param WP_Error $errors               A WP_Error object containing any errors encountered
	 *                                       during registration.
	 * @param string   $sanitized_user_login User's username after it has been sanitized.
	 * @param string   $user_email           User's email.
	 */
	$errors = apply_filters( 'registration_errors', $errors, $sanitized_user_login, $user_email );

	if ( $errors->get_error_code() )
		return $errors;

	//User_Pass
	$user_pass = wp_generate_password( 12, false);
	//user org pass
	$user_pass = apply_filters( 'fudou_org_generate_password', $user_pass );

	//create_user
	$user_id = wp_create_user( $sanitized_user_login, $user_pass, $user_email );
	if ( ! $user_id ) {
		$errors->add( 'registerfail', sprintf( __( '<strong>ERROR</strong>: Couldn&#8217;t register you... please contact the <a href="mailto:%s">webmaster</a> !' ), get_option( 'admin_email' ) ) );
		return $errors;
	}

	//password_nag
	update_user_option( $user_id, 'default_password_nag', true, true ); //Set up the Password change nag.

	//option
	update_user_meta( $user_id, 'first_name', $first_name );
	update_user_meta( $user_id, 'last_name', $last_name );


	if( $user_zip !='' )
		update_user_meta( $user_id, 'user_zip', $user_zip );

	if( $user_adr !='' )  
		update_user_meta( $user_id, 'user_adr', $user_adr );

	if( $user_tel !='' )
		update_user_meta( $user_id, 'user_tel', $user_tel );


	//会員レベル For Vip
	$fudou_kaiin_level = isset( $_POST['kaiin_type'] ) ? myIsNum_f( $_POST['kaiin_type'] ) : '';
	if( !$fudou_kaiin_level ){
		$fudou_kaiin_level = apply_filters( 'fudou_user_kaiin_level', 1 );
	}
	update_user_meta( $user_id, 'fudou_kaiin_level', $fudou_kaiin_level );


	// IPアドレス
	$ipaddress = $_SERVER["REMOTE_ADDR"];
	if( $ipaddress !='' )
		update_user_meta( $user_id, 'ipaddress', $ipaddress );

	$useragent = esc_attr($_SERVER["HTTP_USER_AGENT"]);
	if( $useragent !='' )
		update_user_meta( $user_id, 'useragent', $useragent );

	$today = date("Y/m/d");	// 2011/04/01
	if( $today != '' )
		update_user_meta( $user_id, 'login_date', $today );

	update_user_meta( $user_id, 'login_count', '0' );

	//show_admin_bar_front false
	update_user_meta( $user_id, 'show_admin_bar_front', 'false' );

	//other org options
	do_action( 'fudou_register_post', $user_id );

/*
	update_user_meta( $user_id, 'nickname', $nickname );
	update_user_meta( $user_id, 'description', $description );
	update_user_meta( $user_id, 'rich_editing', $rich_editing );
	update_user_meta( $user_id, 'comment_shortcuts', $comment_shortcuts );
	update_user_meta( $user_id, 'admin_color', $admin_color );
	update_user_meta( $user_id, 'use_ssl', $use_ssl );
	update_user_meta( $user_id, 'show_admin_bar_front', $show_admin_bar_front );
	update_user_meta( $user_id, 'show_admin_bar_admin', $show_admin_bar_admin );
*/

	//Notify the blog admin of a new user, normally via email
	fudou_new_user_notification( $user_id, $user_pass );

	return $user_id;
}





/**
 * Notify the blog admin of a new user, normally via email.
 *
 * @since 2.0
 *
 * @param int $user_id User ID
 * @param string $plaintext_pass Optional. The user's plaintext password
 * wp-includes/pluggable.php
 *
 * Fudousan kaiin Plugin
 * Version: 1.7.12
 */
function fudou_new_user_notification( $user_id, $plaintext_pass = '' ) {

	$user = new WP_User( $user_id );
	$user_login = $user->user_login;
	$user_email = $user->user_email;

	//差出人変更
	$fudoukaiin_wp_mail_from = new fudoukaiin_wp_mail_from();

	// The blogname option is escaped with esc_html on the way into the database in sanitize_option
	// we want to reverse this for the plain text arena of emails.
	$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

	$message  = sprintf(__('New user registration on your site %s:'), $blogname) . "\r\n\r\n";
	$message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
	$message .= sprintf(__('Email: %s'), $user_email) . "\r\n";

	@wp_mail(get_option('admin_email'), sprintf(__('[%s] New User Registration'), $blogname), $message);

	if ( empty($plaintext_pass) )
		return;


	$kaiin_users_mail_new_subject  = wp_specialchars_decode( get_option('kaiin_users_mail_new_subject'), ENT_QUOTES );
	$kaiin_users_mail_new__comment = get_option('kaiin_users_mail_new__comment');

	if($kaiin_users_mail_new_subject == '')
		$kaiin_users_mail_new_subject = sprintf(__('[%s] Your username and password'), $blogname);

	if($kaiin_users_mail_new__comment == ''){
		$message  = sprintf(__('Username: %s'), $user_login) . "\r\n";
		$message .= sprintf(__('Password: %s'), $plaintext_pass) . "\r\n";
		$message .= get_bloginfo('url') . "\r\n";
	}else{

		$first_name = get_user_meta( $user_id, 'first_name', true) ;
		$last_name = get_user_meta( $user_id, 'last_name', true) ;
		$user_zip = get_user_meta( $user_id, 'user_zip', true) ;
		$user_adr = get_user_meta( $user_id, 'user_adr', true) ;
		$user_tel = get_user_meta( $user_id, 'user_tel', true) ;

		//置換
		$kaiin_users_mail_new__comment = str_replace("[user_login]",  $user_login , $kaiin_users_mail_new__comment);
		$kaiin_users_mail_new__comment = str_replace("[user_psass]",  $plaintext_pass , $kaiin_users_mail_new__comment);
		$kaiin_users_mail_new__comment = str_replace("[user_mail]" ,  $user_email , $kaiin_users_mail_new__comment);

		$kaiin_users_mail_new__comment = str_replace("[user_zip]", $user_zip, $kaiin_users_mail_new__comment);
		$kaiin_users_mail_new__comment = str_replace("[user_adr]", $user_adr, $kaiin_users_mail_new__comment);
		$kaiin_users_mail_new__comment = str_replace("[user_tel]", $user_tel, $kaiin_users_mail_new__comment);
		if( $last_name && $first_name ){
			$kaiin_users_mail_new__comment = str_replace("[user_name]", $last_name . ' ' . $first_name , $kaiin_users_mail_new__comment);
		}else{
			$kaiin_users_mail_new__comment = str_replace("[user_name]", $user_login , $kaiin_users_mail_new__comment);
		}

		$message = $kaiin_users_mail_new__comment;
	
	}

	//vip
	$fudou_kaiin_level = isset( $_POST['kaiin_type'] ) ? myIsNum_f( $_POST['kaiin_type'] ) : '';
	if( !$fudou_kaiin_level ){
		$fudou_kaiin_level = apply_filters( 'fudou_user_kaiin_level', 1 );
	}
	if( $fudou_kaiin_level == 1  ){
		wp_mail( $user_email,  $kaiin_users_mail_new_subject, $message );
	}else{
		do_action( 'fudou_new_user_notification_vip', $user );
	}
}



//
// Main
//


//差出人変更
$fudoukaiin_wp_mail_from = new fudoukaiin_wp_mail_from();

//SSL
$fudou_ssl_site_url = get_option('fudou_ssl_site_url');
if( $fudou_ssl_site_url !=''){
	$site_url = $fudou_ssl_site_url;
}else{
	$site_url = get_option('siteurl');
}

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'login';
$errors = new WP_Error();

if ( isset($_GET['key']) )
	$action = 'resetpass';

// validate action so as to default to the login screen
if ( !in_array( $action, array( 'logout', 'lostpassword', 'retrievepassword', 'resetpass', 'rp', 'register', 'login', 'retrievepasswordok', 'loginok', 'registerok' ), true ) && false === has_filter( 'login_form_' . $action ) )
	$action = 'login';

nocache_headers();

header('Content-Type: '.get_bloginfo('html_type').'; charset='.get_bloginfo('charset'));

if ( defined( 'RELOCATE' ) && RELOCATE ) { // Move flag is set
	if ( isset( $_SERVER['PATH_INFO'] ) && ($_SERVER['PATH_INFO'] != $_SERVER['PHP_SELF']) )
		$_SERVER['PHP_SELF'] = str_replace( $_SERVER['PATH_INFO'], '', $_SERVER['PHP_SELF'] );

	$url = dirname( set_url_scheme( 'http://' .  $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] ) );
	if ( $url != get_option( 'siteurl' ) )
		update_option( 'siteurl', $url );
}

//Set a cookie now to see if they are supported by the browser.
$secure = ( 'https' === parse_url( wp_login_url(), PHP_URL_SCHEME ) );
setcookie( TEST_COOKIE, 'WP Cookie check', 0, COOKIEPATH, COOKIE_DOMAIN, $secure );
if ( SITECOOKIEPATH != COOKIEPATH )
	setcookie( TEST_COOKIE, 'WP Cookie check', 0, SITECOOKIEPATH, COOKIE_DOMAIN, $secure );

/**
 * Fires when the login form is initialized.
 *
 * @since 3.2.0
 */
do_action( 'login_init' );
/**
 * Fires before a specified login form action.
 *
 * The dynamic portion of the hook name, `$action`, refers to the action
 * that brought the visitor to the login form. Actions include 'postpass',
 * 'logout', 'lostpassword', etc.
 *
 * @since 2.8.0
 */
do_action( 'login_form_' . $action );

$http_post = ('POST' == $_SERVER['REQUEST_METHOD']);

switch ($action) {

case 'logout' :
	check_admin_referer('log-out');
	wp_logout();

	$redirect_to = !empty( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : $site_url .'/wp-content/plugins/fudoukaiin/wp-login.php?loggedout=true';
	wp_safe_redirect( $redirect_to );
	exit();

case 'lostpassword' :
case 'retrievepassword' :

	if ( $http_post ) {
		$errors = fudoukaiin_retrieve_password();
		if ( !is_wp_error($errors) ) {
			$redirect_to = !empty( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : $site_url .'/wp-content/plugins/fudoukaiin/wp-login.php?checkemail=confirm';
			wp_safe_redirect( $redirect_to );
			exit();
		}
	}

	if ( isset( $_GET['error'] ) ) {
		if ( 'invalidkey' == $_GET['error'] ) {
			$errors->add( 'invalidkey', __( 'Your password reset link appears to be invalid. Please request a new link below.' ) );
		} elseif ( 'expiredkey' == $_GET['error'] ) {
			$errors->add( 'expiredkey', __( 'Your password reset link has expired. Please request a new link below.' ) );
		}
	}

	$lostpassword_redirect = ! empty( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : $site_url . '/wp-content/plugins/fudoukaiin/wp-login.php?action=retrievepasswordok';
	/**
	 * Filters the URL redirected to after submitting the lostpassword/retrievepassword form.
	 *
	 * @since 3.0.0
	 *
	 * @param string $lostpassword_redirect The redirect destination URL.
	 */
	$redirect_to = apply_filters( 'lostpassword_redirect', $lostpassword_redirect );


	/**
	 * Fires before the lost password form.
	 *
	 * @since 1.5.1
	 */
	do_action( 'lost_password' );

	fudoukaiin_login_header(__('Lost Password'), '<p class="message">' . __('Please enter your username or email address. You will receive a link to create a new password via email.') . '</p>', $errors);

	$user_login = isset($_POST['user_login']) ? wp_unslash($_POST['user_login']) : '';

?>
	<style type="text/css" media="all">
		body {	    padding-top: 10px;	}
		.login #login { margin: 0px auto; }
	</style>

<form name="lostpasswordform" id="lostpasswordform" action="<?php echo $site_url; ?>/wp-content/plugins/fudoukaiin/wp-login.php?action=lostpassword" method="post">
	<p>
		<label for="user_login" ><?php _e('Username or Email') ?><br />
		<input type="text" name="user_login" id="user_login" class="input" value="<?php echo esc_attr($user_login); ?>" size="20" tabindex="10" /></label>
		<input type="text" name="mail" value="" style="display:none;" />
	</p>
	<?php
	/**
	 * Fires inside the lostpassword form tags, before the hidden fields.
	 *
	 * @since 2.1.0
	 */
	do_action( 'lostpassword_form' ); ?>
	<input type="hidden" name="redirect_to" value="<?php echo esc_attr( $redirect_to ); ?>" />
	<p class="submit"><input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="<?php esc_attr_e('Get New Password'); ?>" /></p>
</form>

<p id="nav"></p>
<?php
fudoukaiin_login_footer('user_login');
break;

case 'resetpass' :
case 'rp' :

	list( $rp_path ) = explode( '?', wp_unslash( $_SERVER['REQUEST_URI'] ) );
	$rp_cookie = 'wp-resetpass-' . COOKIEHASH;
	if ( isset( $_GET['key'] ) ) {
		$value = sprintf( '%s:%s', wp_unslash( $_GET['login'] ), wp_unslash( $_GET['key'] ) );
		setcookie( $rp_cookie, $value, 0, $rp_path, COOKIE_DOMAIN, is_ssl(), true );
		wp_safe_redirect( remove_query_arg( array( 'key', 'login' ) ) );
		exit;
	}

	if ( isset( $_COOKIE[ $rp_cookie ] ) && 0 < strpos( $_COOKIE[ $rp_cookie ], ':' ) ) {
		list( $rp_login, $rp_key ) = explode( ':', wp_unslash( $_COOKIE[ $rp_cookie ] ), 2 );
		$user = check_password_reset_key( $rp_key, $rp_login );
		if ( isset( $_POST['pass1'] ) && ! hash_equals( $rp_key, $_POST['rp_key'] ) ) {
			$user = false;
		}
	} else {
		$user = false;
	}

	if ( ! $user || is_wp_error( $user ) ) {
		setcookie( $rp_cookie, ' ', time() - YEAR_IN_SECONDS, $rp_path, COOKIE_DOMAIN, is_ssl(), true );
		if ( $user && $user->get_error_code() === 'expired_key' )
			wp_redirect( site_url( 'wp-login.php?action=lostpassword&error=expiredkey' ) );
		else
			wp_redirect( site_url( 'wp-login.php?action=lostpassword&error=invalidkey' ) );
		exit;
	}

	$errors = new WP_Error();

	if ( isset($_POST['pass1']) && $_POST['pass1'] != $_POST['pass2'] )
		$errors->add( 'password_reset_mismatch', __( 'The passwords do not match.' ) );

	/**
	 * Fires before the password reset procedure is validated.
	 *
	 * @since 3.5.0
	 *
	 * @param object           $errors WP Error object.
	 * @param WP_User|WP_Error $user   WP_User object if the login and reset key match. WP_Error object otherwise.
	 */
	do_action( 'validate_password_reset', $errors, $user );

	if ( ( ! $errors->get_error_code() ) && isset( $_POST['pass1'] ) && !empty( $_POST['pass1'] ) ) {
		reset_password($user, $_POST['pass1']);
		setcookie( $rp_cookie, ' ', time() - YEAR_IN_SECONDS, $rp_path, COOKIE_DOMAIN, is_ssl(), true );
		fudoukaiin_login_header( __( 'Password Reset' ), '<p class="message reset-pass">パスワードが登録されました。<br /> <a href="' . esc_url( home_url( '/' ) ) . '">トップページ</a></p>' );
		fudoukaiin_login_footer();
		exit;
	}

	wp_enqueue_script('utils');
	wp_enqueue_script('user-profile');

	fudoukaiin_login_header(__('Reset Password'), '<p class="message reset-pass">' . __('Enter your new password below.') . '</p>', $errors );
?>

<form name="resetpassform" id="resetpassform" action="<?php echo $site_url . '/wp-content/plugins/fudoukaiin/wp-login.php?action=resetpass'; ?>" method="post" autocomplete="off">

	<input type="hidden" id="user_login" value="<?php echo esc_attr( $rp_login ); ?>" autocomplete="off" />

	<div class="user-pass1-wrap">
		<p>
			<label for="pass1"><?php _e( 'New password' ) ?></label>
		</p>

		<div class="wp-pwd">
			<span class="password-input-wrapper">
				<input type="password" data-reveal="1" data-pw="<?php echo esc_attr( wp_generate_password( 16 ) ); ?>" name="pass1" id="pass1" class="input" size="20" value="" autocomplete="off" aria-describedby="pass-strength-result" />
			</span>
			<div id="pass-strength-result" class="hide-if-no-js" aria-live="polite"><?php _e( 'Strength indicator' ); ?></div>
		</div>
	</div>

	<p class="user-pass2-wrap">
		<label for="pass2"><?php _e( 'Confirm new password' ) ?></label><br />
		<input type="password" name="pass2" id="pass2" class="input" size="20" value="" autocomplete="off" />
	</p>

	<p class="description indicator-hint"><?php echo wp_get_password_hint(); ?></p>
	<br class="clear" />

	<?php
	/**
	 * Fires following the 'Strength indicator' meter in the user password reset form.
	 *
	 * @since 3.9.0
	 *
	 * @param WP_User $user User object of the user whose password is being reset.
	 */
	do_action( 'resetpass_form', $user );
	?>
	<input type="hidden" name="rp_key" value="<?php echo esc_attr( $rp_key ); ?>" />
	<p class="submit"><input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="パスワードを決定" /></p>
</form>

	<p id="nav">
	<a href="<?php echo get_option('siteurl'); ?>/wp-content/plugins/fudoukaiin/wp-login.php"><?php _e('Log in') ?></a>
	<?php if (get_option('kaiin_users_can_register')) : ?>
	 | <a href="<?php echo $site_url; ?>/wp-content/plugins/fudoukaiin/wp-login.php?action=register"><?php _e('Register') ?></a>
	<?php endif; ?>
	</p>

	<?php
	fudoukaiin_login_footer('user_pass');
	break;

case 'register' :
	if ( is_multisite() ) {
		/**
		 * Filters the Multisite sign up URL.
		 *
		 * @since 3.0.0
		 *
		 * @param string $sign_up_url The sign up URL.
		 */
		wp_redirect( apply_filters( 'wp_signup_location', network_site_url( 'wp-signup.php' ) ) );
		exit;
	}

	if( get_option('kaiin_moushikomi') == 1 ){
		exit;
	}

	if ( !get_option('kaiin_users_can_register') ) {
		wp_redirect( $site_url .'wp-content/plugins/fudoukaiin/wp-login.php?registration=disabled' );
		exit();
	}

	$user_login = '';
	$user_email = '';
	$user_email2 = '';

	if ( $http_post ) {
		$user_login  = isset( $_POST['user_login'] )  ? $_POST['user_login'] : '';
		$user_email  = isset( $_POST['user_email'] )  ? $_POST['user_email'] : '';
		$user_email2 = isset( $_POST['user_email2'] ) ? $_POST['user_email2'] : '';
		$kaiin_type  = isset( $_POST['kaiin_type'] ) ?  myIsNum_f( $_POST['kaiin_type'] ) : '';

		if( $user_email2 == '' ){
			$errors = register_new_user_fudou( $user_login, $user_email );
			if ( !is_wp_error($errors) ) {
			//	$redirect_to = !empty( $_POST['redirect_to'] ) ? $_POST['redirect_to'] : 'wp-login.php?checkemail=registered';

			//if( $fudou_ssl_site_url !=''){

				$separator = is_rtl() ? ' &rsaquo; ' : ' &lsaquo; ';
				?><!DOCTYPE html>
				<!--[if IE 8]>
					<html xmlns="http://www.w3.org/1999/xhtml" class="ie8" <?php language_attributes(); ?>>
				<![endif]-->
				<!--[if !(IE 8) ]><!-->
					<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
				<!--<![endif]-->
				<head>
				<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
				<title><?php echo get_bloginfo( 'name', 'display' ) . $separator . '会員登録'; ?></title>
				<link rel="stylesheet" id="login-css"  href="<?php echo $site_url; ?>/wp-content/plugins/fudoukaiin/login.css" type="text/css" media="all" />
				<link rel="stylesheet" id="colors-fresh-css"  href="<?php echo $site_url; ?>/wp-admin/css/colors-fresh.css" type="text/css" media="all" />
				<meta name='robots' content='noindex,nofollow' />
				<?php if ( wp_is_mobile() ) { ?>
					<meta name="viewport" content="width=320; initial-scale=0.9; maximum-scale=1.0; user-scalable=0;" />
					<style type="text/css" media="screen">
						form { margin-left: 0px; }
						#login { margin-top: 20px; }
					</style>
				<?php } elseif ( isset($interim_login) && $interim_login ) { 	?>
					<style type="text/css" media="all">
						body {padding-top: 0px;}
						.login #login { margin: 0px auto; }
					</style>
				<?php
					}
				?>
				</head>
				<body class="login wp-core-ui">
				<div id="login">


				<?php if( $kaiin_type >= 2 ){ ?>
					<p class="message">会員申込を受け付け致しました。<br />ログインのアカウントは 後日 メールでお知らせ致しますのでしばらくお待ちください。<br /></p>
				<?php }else{ ?>
					<p class="message">登録を完了しました。メールを確認してください。<br /></p>
				<?php } ?>

				</div>
				</body>
				</html>
				<?php
				exit();
			//}else{
			//	$redirect_to = site_url('wp-content/plugins/fudoukaiin/wp-login.php?action=registerok');
			//	wp_safe_redirect( $redirect_to );
			//	exit();
			//}

			}
		}
	}

	$redirect_to = $site_url . '/wp-content/plugins/fudoukaiin/wp-login.php?action=registerok';

	fudoukaiin_login_header(__('Registration Form'), '', $errors);
?>

	<p class="message">会員登録</p>

<form name="registerform" id="registerform" action="<?php echo $site_url . '/wp-content/plugins/fudoukaiin/wp-login.php?action=register'; ?>" method="post">

	<p>
		<?php _e('Username') ?> 　<font color="#FF2200">(必須)</font> (半角英数字)<br />
		<input type="text" name="user_login" id="user_login" class="input" value="<?php echo esc_attr(wp_unslash($user_login)); ?>" size="20" tabindex="3" />
	</p>
	<p>
		<?php _e('E-mail') ?>　<font color="#FF2200">(必須)</font> <br />
		<input type="text" name="user_email" id="user_email" class="input" value="<?php echo esc_attr(wp_unslash($user_email)); ?>" size="25" tabindex="4" />
	</p>

	<p style="display:none;">
		<?php _e('E-mail2') ?> 　<font color="#FF2200">(必須)</font> (半角英数字)<br />
		<input type="text" name="user_email2" id="user_email" class="input" value="" size="25" tabindex="-1" />
	</p>

	<?php
	/**
	 * Fires following the 'Email' field in the user registration form.
	 *
	 * @since 2.1.0
	 */
	do_action( 'register_form' );
	?>

	<?php if( get_option('kaiin_kiyaku') != '' && !wp_is_mobile() ){ ?>
		<div id="kaiin_kiyaku">

		<?php
		if( get_option('kaiin_kiyakubr') == '1' ){
			echo nl2br(get_option('kaiin_kiyaku')); 
		}else{
			echo get_option('kaiin_kiyaku'); 
		}
		?>

		</div>
	<?php } ?>

		<br class="clear" />
		<p id="reg_passmail">
		<?php if( get_option('kaiin_kiyaku') != '' ){ ?>
			*会員規約に同意の上登録ボタンを押してください。<br />
		<?php } ?>
			*<?php _e('Registration confirmation will be emailed to you.') ?>
		</p>
		<br class="clear" />
		<input type="hidden" name="redirect_to" value="<?php echo $redirect_to ; ?>" />
		<p class="submit"><input type="submit" name="wp-submit" id="wp-submit" class="button-primary" value="<?php esc_attr_e('Register'); ?>" tabindex="100" /></p>


	<?php if( get_option('kaiin_kiyaku') != '' && wp_is_mobile() ){ ?>
		<br class="clear" />
		<br class="clear" />
		<hr />
		<?php
		if( get_option('kaiin_kiyakubr') == '1' ){
			echo nl2br(get_option('kaiin_kiyaku')); 
		}else{
			echo get_option('kaiin_kiyaku'); 
		}
		?>
	<?php } ?>

</form>

<p id="nav">
<!--
<a href="<?php echo site_url('wp-content/plugins/fudoukaiin/wp-login.php', 'login') ?>"><?php _e('Log in') ?></a> |
-->
<a href="<?php echo $site_url; ?>/wp-content/plugins/fudoukaiin/wp-login.php?action=lostpassword" title="<?php _e('Password Lost and Found') ?>"><?php _e('Lost your password?') ?></a>
</p>

<script language="javascript">
	jQuery(function(){
		jQuery("input"). keydown(function(e) {
			if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
				return false;
			} else {
				return true;
			};
		});
	});
</script>

<?php
	fudoukaiin_login_footer('user_login');
	break;

case 'loginok' :
	fudoukaiin_login_header('ログイン', '', $errors);
	echo '<p class="message">ログインしました。<br /></p>';

	fudoukaiin_login_footer('user_login');
	break;

case 'registerok' :
	fudoukaiin_login_header('会員登録', '', $errors);
	echo '<p class="message">登録を完了しました。メールを確認してください。<br /></p>';
	fudoukaiin_login_footer('user_login');
	break;

case 'retrievepasswordok' :
	fudoukaiin_login_header(__('Registration Form'), '', $errors);
	echo '<p class="message">確認用のリンクをメールで送信しました。ご確認ください。<br /></p>';
	fudoukaiin_login_footer('user_login');
	break;


case 'login' :
default:
	$secure_cookie = '';
	$interim_login = isset($_REQUEST['interim-login']);

	/*
	 * Remove Siteguard Captcha
	*/
	global $siteguard_captcha;
	remove_filter( 'login_form', array( $siteguard_captcha, 'handler_login_form' ) );
	remove_filter( 'wp_authenticate_user', array( $siteguard_captcha, 'handler_wp_authenticate_user' ), 1 );


	// If the user wants ssl but the session is not ssl, force a secure cookie.
	if ( !empty($_POST['log']) && !force_ssl_admin() ) {
		$user_name = sanitize_user($_POST['log']);
		$user = get_user_by( 'login', $user_name );

		if ( ! $user && strpos( $user_name, '@' ) ) {
			$user = get_user_by( 'email', $user_name );
		}

		if ( $user ) {
			if ( get_user_option('use_ssl', $user->ID) ) {
				$secure_cookie = true;
				force_ssl_admin(true);
			}
		}
	}

	/*
	if ( isset( $_REQUEST['redirect_to'] ) ) {
		$redirect_to = $_REQUEST['redirect_to'];
		// Redirect to https if user wants ssl
		if ( $secure_cookie && false !== strpos($redirect_to, 'wp-admin') )
			$redirect_to = preg_replace('|^http://|', 'https://', $redirect_to);
	} else {

		$redirect_to = get_bloginfo('url');
	}
	*/

	$redirect_to = '';

	$reauth = empty($_REQUEST['reauth']) ? false : true;

	// secure login
	//$user = wp_signon('', $secure_cookie);
	$user = fudoukaiin_login2($secure_cookie);

	if ( empty( $_COOKIE[ LOGGED_IN_COOKIE ] ) ) {
		if ( headers_sent() ) {
			$user = new WP_Error( 'test_cookie', sprintf( __( '<strong>ERROR</strong>: Cookies are blocked due to unexpected output. For help, please see <a href="%1$s">this documentation</a> or try the <a href="%2$s">support forums</a>.' ),
				__( 'https://codex.wordpress.org/Cookies' ), __( 'https://wordpress.org/support/' ) ) );
		} elseif ( isset( $_POST['testcookie'] ) && empty( $_COOKIE[ TEST_COOKIE ] ) ) {
			// If cookies are disabled we can't log in even with a valid user+pass
			$user = new WP_Error( 'test_cookie', sprintf( __( '<strong>ERROR</strong>: Cookies are blocked or not supported by your browser. You must <a href="%s">enable cookies</a> to use WordPress.' ),
				__( 'https://codex.wordpress.org/Cookies' ) ) );
		}
	}


	if ( !is_wp_error($user) && !$reauth ) {
		if ( $interim_login ) {
			$message = '<p class="message">' . __('You have logged in successfully.') . '</p>';
			$interim_login = 'success';
			fudoukaiin_login_header( '', $message ); ?>
			</div>
			<script type="text/javascript">setTimeout( function(){window.close()}, 8000);</script>
			<p class="alignright">
				<input type="button" class="button-primary" value="<?php esc_attr_e('Close'); ?>" onclick="window.close()" /></p>
			</body></html>
			<?php
			exit;
		}

		if (  empty( $redirect_to ) ) {
			$redirect_to = $site_url . '/wp-content/plugins/fudoukaiin/wp-login.php?action=loginok';
		}
		wp_safe_redirect($redirect_to);
		exit();
	}

	$errors = $user;
	// Clear errors if loggedout is set.
	if ( !empty($_GET['loggedout']) || $reauth )
		$errors = new WP_Error();

	if ( $interim_login ) {
		if ( ! $errors->get_error_code() )
			$errors->add( 'expired', __( 'Your session has expired. Please log in to continue where you left off.' ), 'message' );
	} else {

		// Some parts of this script use the main login form to display a message
		if		( isset($_GET['loggedout']) && true == $_GET['loggedout'] )
			$errors->add('loggedout', __('You are now logged out.'), 'message');
		elseif	( isset($_GET['registration']) && 'disabled' == $_GET['registration'] )
			$errors->add('registerdisabled', __('User registration is currently not allowed.'));
		elseif	( isset($_GET['checkemail']) && 'confirm' == $_GET['checkemail'] )
			$errors->add('confirm', __('Check your email for the confirmation link.'), 'message');
		elseif	( isset($_GET['checkemail']) && 'newpass' == $_GET['checkemail'] )
			$errors->add('newpass', __('Check your email for your new password.'), 'message');
		elseif	( isset($_GET['checkemail']) && 'registered' == $_GET['checkemail'] )
			$errors->add('registered', __('Registration complete. Please check your email.'), 'message');
		elseif ( strpos( $redirect_to, 'about.php?updated' ) )
			$errors->add('updated', __( '<strong>You have successfully updated WordPress!</strong> Please log back in to see what&#8217;s new.' ), 'message' );
	}

	/**
	 * Filters the login page errors.
	 *
	 * @since 3.6.0
	 *
	 * @param object $errors      WP Error object.
	 * @param string $redirect_to Redirect destination URL.
	 */
	$errors = apply_filters( 'wp_login_errors', $errors, $redirect_to );

	// Clear any stale cookies.
	if ( $reauth )
		wp_clear_auth_cookie();

	fudoukaiin_login_header(__('Log In'), '', $errors);

	if ( isset($_POST['log']) )
		$user_login = ( 'incorrect_password' == $errors->get_error_code() || 'empty_password' == $errors->get_error_code() ) ? esc_attr(wp_unslash($_POST['log'])) : '';
	$rememberme = ! empty( $_POST['rememberme'] );

	//$redirect_to = get_bloginfo('wpurl') . '/wp-content/plugins/fudoukaiin/wp-login.php?action=loginok';
	if ( ! empty( $errors->errors ) ) {
		$aria_describedby_error = ' aria-describedby="login_error"';
	} else {
		$aria_describedby_error = '';
	}
?>

<form name="loginform" id="loginform" action="<?php echo site_url('wp-content/plugins/fudoukaiin/wp-login.php', 'login_post') ?>" method="post">
	<p>
		<label><?php _e('Username') ?><br />
		<input type="text" name="log" id="user_login" class="input" value="<?php echo esc_attr(wp_unslash($user_login)); ?>" size="20" tabindex="10" /></label>
	</p>
	<p>
		<label><?php _e('Password') ?><br />
		<input type="password" name="pwd" id="user_pass" class="input" value="" size="20" tabindex="20" /></label>
	</p>
	<p style="display:none;">
		<label><?php _e('mail') ?><br />
		<input type="text" name="mail" id="user_mail" class="input" value="" size="20" tabindex="20" /></label>
	</p>
	<?php
	/**
	 * Fires following the 'Password' field in the login form.
	 *
	 * @since 2.1.0
	 */
	//do_action( 'login_form' );
	?>
	<?php
		//verify_nonce
		echo '<input type="hidden" name="fudou_login_nonce" value="' .wp_create_nonce( 'fudou_login_nonce' ) . '" />';
		echo '<input type="hidden" name="admin_login_nonce" value="' .wp_create_nonce( 'admin_login_nonce' ) . '" />'; //demo site
	?>
	<p class="forgetmenot"><label><input name="rememberme" type="checkbox" id="rememberme" value="forever" tabindex="90"<?php checked( $rememberme ); ?> /> <?php esc_attr_e('Remember Me'); ?></label></p>
	<p class="submit">
			<input type="submit" name="wp-submit" id="wp-submit" class="button-primary" value="<?php esc_attr_e('Log In'); ?>" tabindex="100" />
<?php	if ( $interim_login ) { ?>
		<input type="hidden" name="interim-login" value="1" />
<?php	} else { ?>
		<input type="hidden" name="redirect_to" value="<?php echo esc_attr($redirect_to); ?>" />
<?php 	} ?>
		<input type="hidden" name="testcookie" value="1" />
	</p>
</form>

<?php if ( ! $interim_login ) { ?>
<p id="nav">
<?php if ( isset($_GET['checkemail']) && in_array( $_GET['checkemail'], array('confirm', 'newpass') ) ) : ?>

<?php elseif ( get_option('kaiin_users_can_register') ) : ?>

	<?php if( get_option('kaiin_moushikomi') != 1 ){ ?>
		<a href="<?php echo $site_url; ?>/wp-content/plugins/fudoukaiin/wp-login.php?action=register"><?php _e('Register') ?></a> |
	<?php } ?>

	<a href="<?php echo $site_url; ?>/wp-content/plugins/fudoukaiin/wp-login.php?action=lostpassword" title="<?php _e('Password Lost and Found') ?>"><?php _e('Lost your password?') ?></a>
<?php else : ?>
	<a href="<?php echo $site_url; ?>/wp-content/plugins/fudoukaiin/wp-login.php?action=lostpassword" title="<?php _e('Password Lost and Found') ?>"><?php _e('Lost your password?') ?></a>
<?php endif; ?>

</p>
</div>
<!--
<p id="backtoblog"><a href="<?php bloginfo('url'); ?>/" title="<?php esc_attr_e('Are you lost?') ?>"><?php printf(__('&larr; Back to %s'), get_bloginfo('title', 'display' )); ?></a></p>
-->
<?php } else { ?>
</div>
<?php } ?>

<script type="text/javascript">
function wp_attempt_focus(){
setTimeout( function(){ try{
<?php if ( $user_login ) { ?>
d = document.getElementById('user_pass');
d.value = '';
<?php } else { ?>
d = document.getElementById('user_login');
<?php if ( 'invalid_username' == $errors->get_error_code() ) { ?>
if( d.value != '' )
d.value = '';
<?php
}
}?>
d.focus();
d.select();
} catch(e){}
}, 200);
}

<?php if ( !$error ) { ?>
wp_attempt_focus();
<?php } ?>
if(typeof wpOnload=='function')wpOnload();
<?php if ( $interim_login ) { ?>
(function(){
try {
	var i, links = document.getElementsByTagName('a');
	for ( i in links ) {
		if ( links[i].href )
			links[i].target = '_blank';
	}
} catch(e){}
}());
<?php } ?>
</script>

<?php
fudoukaiin_login_footer('user_login');
break;
} // end action switch



/*
 * 会員ログイン
 *
 * ver 1.7.13
*/
function fudoukaiin_login2( $secure_cookie ){

	$user_login	= isset($_POST['log']) ? esc_attr(wp_unslash($_POST['log'])) : '';
	$password	= isset($_POST['pwd']) ? $_POST['pwd'] : '';
	$rememberme	= isset($_POST['rememberme']) ? true : false;
	$user_mail	= isset($_POST['mail']) ? $_POST['mail'] : '';	//Dummy

	/*
	 * fudoukaiin_login_fix
	 *
	 * ver 1.7.13
	*/
	$user_login = apply_filters( 'fudoukaiin_login_fix', $user_login );

	//Empty USER AGENT
	$useragent = esc_attr($_SERVER["HTTP_USER_AGENT"]);
	if ( empty($useragent) ){
		return new WP_Error('login error', __('login error3'));
	}

	//verify_nonce
	$login_nonce = isset($_POST['fudou_login_nonce']) ?  $_POST['fudou_login_nonce'] : '';
	if ( $user_login && !wp_verify_nonce( $login_nonce, 'fudou_login_nonce') ){
		return new WP_Error('login error', __('login error5'));
	}

	//該当ユーザーの権限
	$user = get_user_by('login', $user_login );
	$user_contributor	= isset( $user->caps['contributor'] ) ?		$user->caps['contributor']  : 0;	//寄稿者
	$user_author		= isset( $user->caps['author'] ) ?		$user->caps['author']  : 0;		//投稿者
	$user_editor		= isset( $user->caps['editor'] ) ?		$user->caps['editor']  : 0;		//編集者
	$user_administrator	= isset( $user->caps['administrator'] ) ?	$user->caps['administrator']  : 0;	//管理者

	//該当ユーザーの権限
	if (( $user_contributor + $user_author + $user_editor + $user_administrator ) > 0 ) {
		return new WP_Error('login error', __('authority error'));
	}else{
		if ( $user_login && $password && $user_mail=='' ) {
			$creds = array();
			$creds['user_login'] = $user_login;
			$creds['user_password'] = $password;
			$creds['remember'] = $rememberme;
			$user = wp_signon( $creds, $secure_cookie );
			return $user;
		}else{
			return new WP_Error('','');
		}
	}
}

