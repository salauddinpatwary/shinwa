<?php
/*
Plugin Name: Fudousan kaiin Plugin
Plugin URI: http://nendeb.jp/
Description: Fudousan kaiin Plugin for Real Estate
Version: 1.7.14
Author: nendeb
Author URI: http://nendeb.jp/
*/

// Define current version constant
define( 'FUDOU_KAIIN_VERSION', '1.7.14' );


/*  Copyright 2017 nendeb (email : nendeb@gmail.com )

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/


if (!defined('WP_CONTENT_URL'))
      define('WP_CONTENT_URL', get_option('siteurl').'/wp-content');
if (!defined('WP_CONTENT_DIR'))
      define('WP_CONTENT_DIR', ABSPATH.'wp-content');
if (!defined('WP_PLUGIN_URL'))
      define('WP_PLUGIN_URL', WP_CONTENT_URL.'/plugins');
if (!defined('WP_PLUGIN_DIR'))
      define('WP_PLUGIN_DIR', WP_CONTENT_DIR.'/plugins');

require_once 'admin_fudoukaiin.php';
require_once 'fudoukaiin-register.php';
require_once 'widget/fudo-widget_kaiin.php';


/*
 * show_admin_bar_front false
*/
//add_filter( 'show_admin_bar', '__return_false' );


//顧客情報追加入力
add_action('register_form', 'fudou_registration_form', 9);


/*
 * 会員 環境 チェック
 * @since Fudousan Kaiin Plugin 1.7.9
*/
function fudou_kaiin_warnings() {

	global $fudou_kaiin_version_err;

	if ( is_multisite() ) {
		function fudou_kaiin_multi_site_notices() {
			echo '<div class="error" style="text-align: center;"><p>マルチサイトでは利用できません。</p></div>';
		}
		add_action( 'admin_notices', 'fudou_kaiin_multi_site_notices' );
	}

	//不動産プラグインシリーズバージョンチェック
		//if ( defined( 'FUDOU_VERSION' ) ) {		if ( version_compare( FUDOU_VERSION		, '1.7.8', '<') )	{ $fudou_kaiin_version_err .= ' 不動産プラグイン(本体) を	ver1.7.8以降 にバージョンアップしてください。		(現在:'	. FUDOU_VERSION . ')<br />' ; } }
		if ( defined( 'FUDOU_MAIL_VERSION' ) ) {	if ( version_compare( FUDOU_MAIL_VERSION	, '1.7.9', '<') )	{ $fudou_kaiin_version_err .= ' 不動産マッチングメールプラグインを	ver1.7.9以降 にバージョンアップしてください。	(現在:' . FUDOU_MAIL_VERSION . ')<br />' ; } }

		function fudou_kaiin_version_err_notices( $fudou_kaiin_version_err ) {
			global $fudou_kaiin_version_err;
			echo '<div class="error" style="text-align: center;"><p>不動産会員プラグインを利用するには、以下の不動産プラグインシリーズをバージョンアップしてください。<br />' . $fudou_kaiin_version_err . '</p></div>';
		}
		if( $fudou_kaiin_version_err ){
			add_action( 'admin_notices', 'fudou_kaiin_version_err_notices' );
		}

}
add_action('admin_init', 'fudou_kaiin_warnings' );


/**
 *
 * 不動産会員プラグインチェック
 *
 * @since Fudousan kaiin Plugin 1.7.0
 */
function fudou_active_plugins_check_fudoukaiin(){
	global $is_fudoukaiin;
	$is_fudoukaiin = true;
}
add_action('init', 'fudou_active_plugins_check_fudoukaiin');



// 会員ログイン・ログアウト
function fudoukaiin_main(){
	$fudoukaiin = isset($_POST['md']) ? esc_attr( stripslashes( $_POST['md'])) : null;
	if($fudoukaiin == '')
		$fudoukaiin = isset($_GET['md']) ? esc_attr( stripslashes( $_GET['md'])) : null;
	switch ($fudoukaiin) {
		case ("login"):
			fudoukaiin_login();
			break;

		case ("logout"):
			fudoukaiin_logout();
			break;
	}
}
add_action('init', 'fudoukaiin_main');


//thickbox
function fudoukaiin_thickbox(){
	if (function_exists('add_thickbox')) add_thickbox();
}
add_action('init', 'fudoukaiin_thickbox');


/**
 * 会員ログイン
 * 
 * @since Fudousan kaiin Plugin 1.7.14
 */
function fudoukaiin_login(){

	/*
	 * Remove Siteguard Captcha
	*/
	global $siteguard_captcha;
	//remove_filter( 'login_form', array( $siteguard_captcha, 'handler_login_form' ) );
	remove_filter( 'wp_authenticate_user', array( $siteguard_captcha, 'handler_wp_authenticate_user' ), 1 );


	$redirect_to = isset($_POST['redirect_to']) ? $_POST['redirect_to'] : '';
	if (!$redirect_to) {
		$redirect_to = get_bloginfo('url');
	}

	$user_mail   = isset($_POST['mail']) ? $_POST['mail'] : '';	//Dummy
	$user_login  = isset($_POST['log'])  ? $_POST['log'] : '';
	$password    = isset($_POST['pwd'])  ? $_POST['pwd'] : '';
	$rememberme  = isset($_POST['rememberme']) ? true : false;
	$login_nonce = isset($_POST['fudou_userlogin_nonce']) ?  $_POST['fudou_userlogin_nonce'] : '';	//verify_nonce

	/*
	 * fudoukaiin_login_fix
	 *
	 * ver 1.7.14
	*/
	$user_login = apply_filters( 'fudoukaiin_login_fix', $user_login );

	//verify_nonce
	if( $user_login && wp_verify_nonce( $login_nonce, 'fudou_userlogin_nonce') ) {
		$user_login = sanitize_user( $user_login );

		//該当ユーザーの権限
		$user = get_user_by('login', $user_login );
		$user_contributor	= isset( $user->caps['contributor'] ) ?		$user->caps['contributor']  : 0;	//寄稿者
		$user_author		= isset( $user->caps['author'] ) ?		$user->caps['author']  : 0;		//投稿者
		$user_editor		= isset( $user->caps['editor'] ) ?		$user->caps['editor']  : 0;		//編集者
		$user_administrator	= isset( $user->caps['administrator'] ) ?	$user->caps['administrator']  : 0;	//管理者

		if (( $user_contributor + $user_author + $user_editor + $user_administrator ) > 0 ) {
		}else{
			if ( $user_login && $password && $user_mail=='' ) {
				$creds = array();
				$creds['user_login'] = $user_login;
				$creds['user_password'] = $password;
				$creds['remember'] = $rememberme;
				$user = wp_signon( $creds, false );
				if ( is_wp_error($user) ){
					//echo $user->get_error_message();
				}else{
					wp_redirect($redirect_to);
					exit();
				}
			}
		}
	}
}


/**
 * 会員ログアウト
 * 
 * @since Fudousan kaiin Plugin v1.7.8
 */
function fudoukaiin_logout(){
	$redirect_to = get_bloginfo('url');

	wp_destroy_current_session();
	wp_clear_auth_cookie();
//	do_action('wp_logout');
	nocache_headers();
	wp_redirect($redirect_to);
	exit();
}


/**
 * 簡易ログ
 * 
 * @since Fudousan kaiin Plugin
 */
function fudoukaiin_userlogin_success($user_id) {
	//日付
	$today = date("Y/m/d");	// 2011/04/01
	$login_date	= get_user_meta( $user_id, 'login_date', true);
	$login_count	= get_user_meta( $user_id, 'login_count', true);
	if($login_count == '') $login_count = 0;

	if( $today != $login_date ){
		$login_count ++ ;
		update_user_meta( $user_id, 'login_count', $login_count );
		update_user_meta( $user_id, 'login_date', $today );
	}else{
		if($login_count == '0')
			update_user_meta( $user_id, 'login_count', '1' );
	}
}




/**
 * パスワード変更
 * 
 * @since Fudousan kaiin Plugin ver1.7.14
 */
function fudoukaiin_repass_user_widget() {

	global $user_ID;
	global $chenged_user_id;

	$repass_md  = isset($_POST['md']) ? esc_attr($_POST['md']) : '';
	$new_pass   = isset($_POST['pass1']) ? trim($_POST['pass1']) : '';
	$new_pass2  = isset($_POST['pass2']) ? trim($_POST['pass2']) : '';

	$repass_user_nonce = isset($_POST['fudou_repass_user_nonce']) ?  $_POST['fudou_repass_user_nonce'] : '';	//verify_nonce

	//verify_nonce
	if( $repass_user_nonce && wp_verify_nonce( $repass_user_nonce, 'fudou_repass_user_nonce') ) {

		if ( $user_ID && $new_pass != '' && $new_pass2 != '' && $repass_md == 'repass' ) {

			if ($new_pass == $new_pass2) {

				//差出人変更
				$fudoukaiin_wp_mail_from = new fudoukaiin_wp_mail_from();

				$userdata['ID'] = $user_ID; //user ID
				$userdata['user_pass'] = $new_pass;
				$chenged_user_id = wp_update_user( $userdata );
			}
		}
	}
}
add_filter( 'after_setup_theme', 'fudoukaiin_repass_user_widget' );






/**
 * 差出人変更
 * 
 * @since Fudousan kaiin Plugin 1.7.7
 */
if ( !class_exists('fudoukaiin_wp_mail_from') ) {

	class fudoukaiin_wp_mail_from {

		function __construct() {

			$kaiin_users_mail_fromname = get_option('kaiin_users_mail_fromname');
			$kaiin_users_mail_frommail = get_option('kaiin_users_mail_frommail');

			if( $kaiin_users_mail_frommail !='' ){
				add_filter( 'wp_mail_from', array( $this, 'fb_mail_from' ) );
			}
			if( $kaiin_users_mail_fromname !='' ){
				add_filter( 'wp_mail_from_name', array( $this, 'fb_mail_from_name' ) );
			}

			//Fires after PHPMailer is initialized. 
			add_action( 'phpmailer_init', array( $this, 'fb_mail_sender' ), 20 );

		}

		 // new name
		function fb_mail_from_name() {
			$kaiin_users_mail_fromname = get_option('kaiin_users_mail_fromname');
			$name = $kaiin_users_mail_fromname;
			// alternative the name of the blog
			// $name = get_option('blogname');
			$name = esc_attr($name);
			return $name;
		}

		 // new email-adress
		function fb_mail_from() {
			$kaiin_users_mail_frommail = get_option('kaiin_users_mail_frommail');
			$email = $kaiin_users_mail_frommail;
			$email = is_email($email);
			return $email;
		}

		/**
		 * Fires after PHPMailer is initialized. 
		 * @param PHPMailer &$phpmailer The PHPMailer instance, passed by reference. 
		 * 
		 * @since Fudousan kaiin Plugin 1.7.7
		 */
		function fb_mail_sender( $phpmailer ) {
			$kaiin_users_mail_sender   = get_option('kaiin_users_mail_sender');
			if( $kaiin_users_mail_sender ){
				$phpmailer->Sender = $kaiin_users_mail_sender;
			}
		}
	}
	//$wp_mail_from = new fudoukaiin_wp_mail_from();
}



/*
 * 管理者は会員物件を表示
 *
 * apply_filters( 'users_kaiin_bukkenlist', $view, $post_id, $kaiin_users_rains_register, $kaiin );
 *
 * fudou/admin/fudo-functions.php
 * fudouktai/fudousan/functions.php
 * Version: 1.7.9
 */
function admin_kaiin_bukkenlist_view( $view, $post_id, $kaiin_users_rains_register, $kaiin ){

	//管理者
	if ( current_user_can( 'edit_posts' ) ) {
		$view = true;
	}
	return $view;
}
add_filter( 'users_kaiin_bukkenlist', 'admin_kaiin_bukkenlist_view', 11, 4 );



/**
 * Filters the list of blacklisted usernames.
 *
 * @since 4.4.0
 * Version: 1.7.14
 * @param array $usernames Array of blacklisted usernames.
 */
function fudoukaiin_illegal_user_logins( $illegal_logins ){

	$illegal_names = array( 'www', 'web', 'root', 'admin', 'main', 'invite', 'administrator' );
	$illegal_logins = array_merge( $illegal_logins, $illegal_names );

	return $illegal_logins;
}
add_filter( 'illegal_user_logins', 'fudoukaiin_illegal_user_logins' );



/**
 * 会員(購読者)は ダッシュボードを見せない.
 * Fires before the authentication redirect.
 *
 * @since 2.8.0
 * Version: 1.7.14
 *
 * @param int $user_id User ID.
 */
function fudoukaiin_subscribers_dashboard_redirect( $user_id ) {

	$check_user = new WP_User( $user_id );
	if( isset( $check_user->roles[0] ) ){
		$user_roles = $check_user->roles[0];	//subscriber
	}else{
		$user_roles = '';
	}

	if( $user_roles == 'subscriber' ){
		wp_redirect( home_url() );
		exit();
	}
}
add_action( 'auth_redirect', 'fudoukaiin_subscribers_dashboard_redirect' );


/**
 *
 * View Version in Footer
 *
 */
function fudou_kaiin_footer_version() {
    echo "<!-- FUDOU KAIIN VERSION " . FUDOU_KAIIN_VERSION . " -->\n";
}
add_filter( 'wp_footer', 'fudou_kaiin_footer_version' );

