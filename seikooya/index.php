<!DOCTYPE html>
<html lang="ja">
<meta charset="UTF-8">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KT2K25L');</script>
<!-- End Google Tag Manager -->
<meta charset="utf-8">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
$(function(){
    // setViewport
    spView = 'width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=0';
    tbView = 'width=1200px,maximum-scale=2.0,user-scalable=1';
 
    if(navigator.userAgent.indexOf('iPhone') > 0 || navigator.userAgent.indexOf('iPod') > 0 || (navigator.userAgent.indexOf('Android') > 0 && navigator.userAgent.indexOf('Mobile') > 0)){
        $('head').prepend('<meta name="viewport" content="' + spView + '" id="viewport">');
    } else if(navigator.userAgent.indexOf('iPad') > 0 || (navigator.userAgent.indexOf('Android') > 0 && navigator.userAgent.indexOf('Mobile') == -1) || navigator.userAgent.indexOf('A1_07') > 0 || navigator.userAgent.indexOf('SC-01C') > 0){
        $('head').prepend('<meta name="viewport" content="' + tbView + '" id="viewport">');
    } 
 
    // iPhone6 Plus Landscape Viewport
    if(navigator.userAgent.indexOf('iPhone') > 0){
        if(window.devicePixelRatio == 3) {
 
            if(window.orientation == 0){
                $('#viewport').attr('content',spView);
            } else {
                $('#viewport').attr('content',tbView);
            }
 
            window.onorientationchange = setView;
 
            function setView(){
                setTimeout(function(){
                    location.reload();
                    return false;
                },500);
            }
        }
    }
});
</script>
<title>新築成功大家さん｜新築不動産投資で利回り重視の投資にしませんか</title>
<meta name="description" content="新築不動産投資の新築成功大家さんなら土地探しから入居者管理まで全て進和グループが行うので、中間コストが抑えられ効率的な不動産投資利回りを実現。投資プランを無料でおつくりします！お手元の投資プランと新築成功大家さんの投資プランを比較してください！" />
<meta property="og:title" content="新築成功大家さん｜新築不動産投資で利回り重視の投資にしませんか" />
<meta property="og:description" content="新築不動産投資の新築成功大家さんなら土地探しから入居者管理まで全て進和グループが行うので、中間コストが抑えられ効率的な不動産投資利回りを実現。投資プランを無料でおつくりします！お手元の投資プランと新築成功大家さんの投資プランを比較してください！">
<meta property="og:type" content="website">
<meta property="og:url" content="http://www.shinwa-f.jp/seikooya/" />
<meta property="og:image" content="images/common/ogp.jpg" />
<meta name="twitter:card" content="summary_large_image">
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta name="viewport" content="width=device-width; initial-scale=1.0" />
<link rel="stylesheet" type="text/css" href="css/html5-doctor.css" />
<link rel="stylesheet" type="text/css" href="css/layout.css" />
<link rel="stylesheet" type="text/css" href="css/pagetop.css" />
<link rel="stylesheet" type="text/css" href="css/sp_menu.css" />
<!--[if lt IE 9]> 
<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script> 
<![endif]-->  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript" src="js/breakpoints.js"></script>
<script type="text/javascript" src="js/img_change.js"></script>
<!-- ページトップ -->
<script type="text/javascript" src="js/pagetop.js"></script>
<script type="text/javascript" src="js/nav_fix.js"></script>
<script type="text/javascript" src="js/accordion.js"></script>

<!-- ページトップend -->

</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KT2K25L"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="wrap">
	<div class="side-fix sp-none">
		<a href="contact.html" target="_blank"><img src="images/picture/side_1.png" alt=""></a>
		<img src="images/picture/side_2.png" alt="Tel.072-256-6210">
	</div>

	<section class="top-banner">
		<img src="images/picture/banner_top_pc.png" alt="新築投資" class="sp-img">
	</section>
	
	<section class="section-1">
		<h3 class="tt"><img src="images/title/sec1_tt_pc.jpg" alt="土地探しから、新築建設、入居者・運営会社確保、物件管理までトータルにサポート!" class="sp-img"></h3>
		<div class="content">
			<div class="container">
				<img src="images/picture/sec1_01_pc.png" alt="新築成功大家さん" class="sp-img">
			</div>
		</div>
	</section>
	
	<section class="section-2">
		<div class="container">
			<div class="icon center"><img src="images/icon/icon_1.png" alt="icon1" class="sp-img"></div>
			<h2><span class="img">新築成功大家さん</span>が<br>新築不動産<span>投資</span>パートナーに選ばれる<img src="images/icon/sec2_icon.png" alt="４つの理由" class="sp-img"></h2>
			<div class="box-wrap">
				<div class="item">
					<div class="ribbon">
						<img src="images/icon/ribbon_1.png" alt="理由１" class="sp-img">
					</div>
					<h3 class="tt">不動産経営の最高資格保持者が提案</h3>
					<div class="txt clearfix">
						<div class="img">
							<img src="images/picture/sec2_01_pc.png" alt="西田泰久" class="sp-none">
						</div>
						<h3>米国公認不動産経営管理士の西田泰久が<br class="sp-none">300件を超える投資成功実績をもとにプランをご提案します。</h3>
						<p>不動産投資セミナー年間１２回以上実施、過去の投資成功事例３００件！を超える専門コンサルタントが最適なプランをご提案します。<br>
						不動産投資はオーナー様自身が投資の内容が理解出来ていない方も多くみられるのが現状です。そのような中、私は一人一人に対しての“土地を活用する意味・不動産投資をする意味”にこだわっていきたいと思います。お客様のご家族、資産内容から、“お客様にとって一番大切なことは何なのか？”“お客様にとっての幸せは一体何なのか？”ということをずっと心に持ち続けていたいと思っております。<br>
						誤解しないでください！私たちは不動産を売りつけることが仕事ではありません。「お宝物件」をご紹介し、お客様の資産運用がうまくいくようにサポートすることが仕事であり「使命」です。ですから、無理に物件を押し付けることもありません。成功しないと思えばはっきりそう言います。<br>
						私たちは、お客様との「一生涯のお付き合い」を願っています。なぜなら、一度不動産投資に成功すると、二度目、三度目・・・とリピーターとなる方が多いからです。それが私たちの利益にもなりますから、是が非でもお客様には成功して頂かなければなりません。<br>
						相続対策から節税対策、不動産投資までお気軽にご相談下さい。</p>
						<div class="img">
						<img src="images/picture/sec2_01_sp.png" alt="西田泰久" class="pc-none"></div>
					</div>
				</div>

				<div class="item">
					<div class="ribbon">
						<img src="images/icon/ribbon_2.png" alt="理由2" class="sp-img">
					</div>
					<h3 class="tt">効率の良い投資が可能</h3>
					<div class="txt">
						<h3>土地探しから建設、入居者手配、物件管理、投資運用改善まで全てグループ内で簡潔！<br>だから、中間コストが削減できて効率の良い投資が可能です。</h3>
						<p>例えば当社の実績ですと、投資額約3億5千万円の場合、約5千万円のコストカット可能です。大手投資パートナーさんの場合は、プラン立て、土地探し、建設、物件管理が別会社になる場合が多いので、私どもが同じ土地に対してプランをご提案した場合、トータルコストが削減でき、投資シュミレーションの差に驚いていただけることが多いです。</p>
					</div>
				</div>

				<div class="item">
					<div class="ribbon">
						<img src="images/icon/ribbon_3.png" alt="理由3" class="sp-img">
					</div>
					<h3 class="tt">ハイコンセプトな企画設計・自社施工で<br>利回り最優先</h3>
					<div class="txt">
						<h3>コンセプト型マンションでグループ内物件、入居率<span>96%</span>※を実現！</h3>
						<p>当社では、入居者ターゲットを絞ったマンションのプランニングを行っています。例えば、女性、ペットを飼っている（飼いたい）人、小さなお子さんのいる人、健康に気を遣っている人・・・などしっかりしたコンセプトを持ったマンションで「満室経営」を目指しています。<br>
						マンションが供給過剰、つまり有り余っている中で入居者に選んでもらうためには、他とは違う“何か”を付加し惹きつける必要があります。ところが、差別化したつもりで差別化できていないマンションが多いために、新築でも空室が続くケースが多く見受けられます。<br>
						その解決方法が、「入居者ターゲットを絞る」ことであり、その元となる「市場調査」を十分に行うことです。<br>
						当社では、土地の周辺調査として人口比率、世帯数、既存賃貸物件とその入居率、施設などを調べ上げ、その結果からその地域の住宅に対する“ニーズ”を見つけ出します。そして、そのニーズに合わせたマンションをプランニングしますので、それ自体がすでに差別化できたものであり、魅力あるマンションとなっているのです。<br>
						この「入居者ターゲットを絞る」ことのメリットとして、「家賃を高めに設定できる」ということも、利回りが取れる秘訣です。なぜなら、「それだけ払っても入居したい」と入居者に思わせることができるからです。<br>
						※2016年4月実績
						</p>
					</div>
				</div>

				<div class="item">
					<div class="ribbon">
						<img src="images/icon/ribbon_4.png" alt="理由4" class="sp-img">
					</div>
					<h3 class="tt">自社施工で低コストで安心施工</h3>
					<div class="txt">
						<h3>企画・設計から施工まで一貫した体制と、資材の購入方法、工期の短縮によりローコスト化に成功</h3>
						<p>一般的に大手ハウスメーカーは全国展開しているため、施工を独自で行っていない事が少なくありません。その場合、施工は地元の工務店に丸投げしています。ですから、大手ハウスメーカーはこの部分に利益を上乗せしているために建築コストが高くなります。当社は、企画・設計から施工まで一貫した体制と、資材の購入方法、工期の短縮によりローコスト化に成功しています。
							価格・内容共に納得いただけるプランをご提案します。</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	

	<section class="section-3">
		<div class="container">
			<h2><img src="images/icon/icon_2.png" alt="icon2" class="sp-img">新築投資実績</h2>
			<div class="box box1">
				<div class="top sp-none">
					<img src="images/icon/sec3_ribbon_top.png" alt="icon3" class="sp-img">
				</div>
				<div class="bot sp-none">
					<img src="images/icon/sec3_ribbon_bot.png" alt="icon7" class="sp-img">
				</div>
				<h3 class="tt">
					<img src="images/title/sec3_title_01.png" alt="新築マンション投資を行ったM様" class="sp-img">
				</h3>
				<div class="item-img clearfix">
					<h4 class="title icon1 pc-none">新築成功大家さんを<br>利用しようと思ったきっかけ</h4>
					<div class="img">
						<img src="images/picture/sec3_01.png" alt="新築成功大家さんを利用しようと思ったきっかけM" class="sp-img">
					</div>
					<div class="txt">
						<h4 class="title icon1 sp-none">新築成功大家さんを<br>利用しようと思ったきっかけ</h4>
						<p>いくつかマンションを所有し、相続対策はもうある程度できてきたのですが、所得税対策のため、そして将来の資産の形成のために　新しい建物を建てることを検討していました。<br>
						新築成功大家さんを知ったきっかけは日経新聞が主催する相続フォーラムで、西田社長の講演を聞いた時でした。その後　良い土地があったら紹介して欲しいと相談。その時に紹介してもらったのが今回の土地です。</p>
					</div>
				</div>
				<div class="clearfix">
					<div class="img-right sp-none">
						<img src="images/picture/sec3_02.png" alt="提案内容1">
						<img src="images/picture/sec3_03.png" alt="提案内容2">
						<img src="images/picture/sec3_04.png" alt="現在のご状況">
					</div>
					<div class="item bg">
						<h4 class="title icon2">提案内容</h4>
						<div class="img pc-none">
							<img src="images/picture/sec3_02_sp.png" alt="提案内容1">
						</div>
						<p>他社さんにも紹介してもらった土地があったのですが、駅からの距離やロケーション、そしエリアも人気のあるところだったので、やっぱりここが一番でした。<br>
						収支のシュミレーションも出してもらい、土地から購入しても、築後の経営的そして将来的にも良さそうだったので、新築成功大家さんにお願いすることに決めました。</p>
					</div>
					<div class="item">
						<h4 class="title icon3">現在のご状況</h4>
						<div class="img pc-none">
							<img src="images/picture/sec3_04_sp.png" alt="現在のご状況">
						</div>
						<p>完成後　デザインも良く、素晴らしい建物ができたと思います。<br>
						今回はサブリースの契約をしているので経営的にはある程度は安心しているところもあります。<br>
						このマンションは娘みたいなもの。これからも新築成功大家さんとは末永いお付き合いになると思っています。やっぱり自分が選んで間違いなかった、良い会社に出会えたと思っています。</p>
					</div>
				</div>
			</div>

			<div class="box box2">
				<div class="top sp-none">
					<img src="images/icon/sec3_ribbon_top.png" alt="icon3" class="sp-img">
				</div>
				<div class="bot sp-none">
					<img src="images/icon/sec3_ribbon_bot.png" alt="icon7" class="sp-img">
				</div>
				<h3 class="tt">
					<img src="images/title/sec3_title_02.png" alt="新築木造アパート投資を行ったK様" class="sp-img">
				</h3>
				<div class="clearfix">
					<div class="img-right sp-none">
						<img src="images/picture/sec3_05.png" alt="新築成功大家さんを利用しようと思ったきっかけK">
						<img src="images/picture/sec3_06.png" alt="現在のご状況">
					</div>
					<div class="item top0">
						<h4 class="title icon1">新築成功大家さんを<br>利用しようと思ったきっかけ</h4>
						<div class="img pc-none">
							<img src="images/picture/sec3_05_sp.png" alt="新築成功大家さんを利用しようと思ったきっかけK">
						</div>
						<p>現在は東京で会社勤めをしていますが、大阪で生まれ育ったことから大阪で私的年金として収益不動産を所有したいと考えていました。<br>
						中古物件も検討していましたが、知人の紹介で新築成功大家さんの木造アパートを知り、居住性、独自の建物、何より10年先の収入が下がりにくい設計に共感し土地探しからお願いいたしました。</p>
					</div>
					<div class="item bg">
						<h4 class="title icon2">提案内容</h4>
						<p>大阪府の北摂地方で、低層住宅しか建築できない場所でいたが人気のエリアで駅徒歩5分。<br>
						土地を検討していた時は、他社にも同じ土地で提案をお願いしましたが「このエリアはアパート建築できない」また「平凡な間取りで他のアパートと差別化できない提案」などから新築成功大家さんにきめました。</p>
					</div>
					<div class="item">
						<h4 class="title icon3">現在のご状況</h4>
						<div class="img pc-none"><img src="images/picture/sec3_06.png" alt="現在のご状況"></div>
						<p>今回は8室のアパートで物件の引き渡しが、年末だった為、正直春まで満室は難しいだろうと思っていましたが、引渡し後1ヶ月で入居者もすべて決まり順調なアパート経営を始めることが出来ました。<br>
						木造アパートは有名大手企業でも騒音問題など設計上、構造上のトラブルが多いと聞いていましたが私のアパートは現在までそのようなトラブルは起こっていません。先日、ある程度返済が進めば2棟目の融資を検討したいと金融機関から提案をうけました。<br>
						新築成功大家さんを選んで間違いなかったと思っています。</p>
					</div>

				</div>
			</div>

		</div>
	</section>

	<section class="section-4">
		<div class="container">
			<h2>中古マンション投資と新築不動産投資の比較</h2>
			<div class="table">
				<table>
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th>中古マンション</th>
							<th>新築投資</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>人気物件にするための工夫</td>
							<td>リノベーションに限界が･･･思い通りの加工は難しい</td>
							<td>土地の選定・建物の設計段階から売れる物件にするための工夫が可能</td>
						</tr>
						<tr>
							<td>立　地</td>
							<td>狙ったエリアにイメージ通りの物件があるとは限らない</td>
							<td>人気のエリアを狙って賃貸物件を建てられる</td>
						</tr>
						<tr>
							<td>家　賃</td>
							<td>人気のエリアでも年々下降傾向に</td>
							<td>企画次第で相場の2〜3割高い価格設定も</td>
						</tr>
						<tr>
							<td>初期投資費用</td>
							<td>新築に比べて割安</td>
							<td>中古よりも高価に</td>
						</tr>
						<tr>
							<td>利回り</td>
							<td>6〜7%程度だが中古市場は低下傾向に･･･<br>経費を考えると手残り金額はさらに少なく</td>
							<td>企画次第で7〜8％以上の物件も中古に比べ経費も少なく手残り金額も多い</td>
						</tr>
						<tr>
							<td>メンテナンス・修繕</td>
							<td>古い建物ほどコスト増配管など目に見えぬ修繕で思わぬ出費になることも</td>
							<td>コストは当面ほとんどかからない。</td>
						</tr>
						<tr>
							<td>新築特権</td>
							<td>新築特権は得られない</td>
							<td>一定の需要有<br>築後2〜3は満室になりやすい</td>
						</tr>
						<tr>
							<td>売却する時</td>
							<td>築年数にも影響されるが、年々、売却が難しくなる</td>
							<td>良いタイミングで売却し、次の不動産投資の資金にすることも</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		
	</section>

	<section class="section-5">
		<div class="title">
			<h2>新築投資物件、施工事例</h2>
		</div>
		<div class="content">
			<div class="container">
				<h3><span>すべて進和グループ</span><small>の</small><span>自社施工</span><small>のため</small><br>
				一般の不動産会社<small>から</small>紹介<small>される</small>建設会社<small></small>より<span>低価格・高品質</span><small>です。</small>
				</h3>
				<div class="box-wrap clearfix">
					<img src="images/picture/sec5_01_pc.jpg" alt="木造アパート" class="sp-img">
					<img src="images/picture/sec5_02_pc.jpg" alt="ローコストマンション" class="sp-img">
					<img src="images/picture/sec5_03_pc.jpg" alt="サービス付高齢者住宅" class="sp-img">
				</div>
			</div>
		</div>
	</section>

	<section class="section-6">
		<div class="title">
			<h2>新築投資お問い合わせ<small>から</small>運用<small>までの</small><span>流<small>れ</small></span></h2>
			<h3>
				<img src="images/title/sec6_tt_pc.png" alt="" class="sp-img">
			</h3>
		</div>
		<div class="content">
			<div class="container">
				<img src="images/picture/sec6_01_pc.png" alt="新築投資お問い合わせから運用までの流れ" class="sp-img">
				<p>この一連の動きの中で発生する法務・税務、不動産投資に関する様々な疑問・悩みには、各分野における専門家による「サポート集団」がしっかりサポートします。</p>
			</div>
		</div>
		
	</section>


	<!--<section class="section-7">
		<div class="container">
			<div class="title clearfix">
				<img src="images/title/sec7_title.png" alt="セミナー" class="sp-img">
				<h2>不動産投資<small>が</small>初めて<small>の方にも</small><br>
				<span>安心<small>して頂ける</small>セミナー</span><small>があります!</small></h2>
			</div>
			<div class="box-wrap clearfix">
				<div class="seminar"><img src="images/title/seminar.png" alt="Seminar" class="sp-img"></div>
				
				<div class="left">
					<h3>セミナー概要</h3>
					<p>これはダミーテキストとなります。ここには「セミナー概要」テキスト内容が入ります。これはダミーテキストとなります。<br>
					ここには「セミナー概要」のテキスト内容が入ります。これはダミーテキストです。<br>
					これはダミーテキストとなります。ここには「セミナー概要」のテキスト内容が入ります。これはダミーテキストとなります。ここには「セミナー概要」のテキスト内容が入ります。<br>
					これはダミーテキストとなります。ここには「セミナー概要」のテキスト内容が入ります。これはダミーテキストとなります。ここには「セミナー概要」のテキスト内容が入ります。これはダミーテキストです。ここには「セミナー概要」のテキスト内容が入ります。これはダミーテキストです。</p>
				</div>
				<div class="right">
					<h3>講師</h3>
					<div class="clearfix">
						<div class="img">
							<img src="images/picture/sec7_01_pc.jpg" alt="講師">
						</div>
						<p>これはダミーテキストとなります。ここには「プロフィール」のテキスト内容が入ります。これはダミーテキストとなります。ここには「プロフィール」のテキスト内容が入ります。これはダミーテキストとなります。</p>
						<p>これはダミーテキストとなります。ここには「講師」のテキスト内容が入ります。これはダミーテキストとなります。これはダミーテキストとなります。ここには「講師」のテキスト内容が入ります。これはダミーテキストとなります。</p>
					</div>
				</div>
			</div>
			<div class="button sp-none">
				<a href="https://ws.formzu.net/fgen/S11515367/" target="_blank"><img src="images/button/sec7_btn.png" alt="今すぐセミナーに申し込む" class="sp-img"></a>
			</div>
		</div>
	</section>-->

	<section class="section-8">
		<div class="container">
			<img src="images/title/sec8_tt_pc.jpg" alt="具体的な投資プランも無料でお作りしています!" class="sp-img">
		</div>
	</section>

	<!--footer-->
	<footer class="footer">
		<div class="container clearfix">
			<div class="info">
				<h2>お問い合わせ & 運営</h2>
				<h3 class="clearfix"><img src="images/icon/footer_icon.png" alt="icon9" class="sp-img">
					<span>新築成功大家さん</span><br>by 進和グループ
				</h3>
				<p>〒591-8032  大阪府堺市北区百舌鳥梅町1丁目30番地1</p>
			</div>
			<div class="tel sp-none">
				<a href="tel:072-256-6210">
					<img src="images/picture/footer_tel.png" alt="072-256-6210">
				</a>
			</div>
		</div>
	</footer>
	<!--footer-end-->
	<a href="#" class="backtotop sp-none">
		<img src="images/icon/backtotop.png" alt="back top">
	</a>
</div>
<div class="footer_m pc-none">
	<p>新築成功大家さん見たよ！とお伝えください</p>
	<div class="clearfix">
		<a href="tel:072-256-6210">
			<img src="images/picture/contact_sp_1.png" alt="072-256-6210">
		</a>
		<a href="contact.html" target="_blank">
			<img src="images/picture/contact_sp_2.png" alt="お問い合わせはこちら">
		</a>
	</div>
</div>

</body>
</html>