
/* ===== 表示調整 ================================================== */

var block_size = (function() {
	var tgtHei = $(window).height() - $('#footer').outerHeight();
	var cntHei = $('#pagecontent_wrap').height();
	if ( cntHei < tgtHei ) {
		$('#pagecontent').height( tgtHei );
	} else {
		$('#pagecontent').height( cntHei );
	}
});

/* ===== イベント ================================================== */

//最新
var html_include_wp = (function( pnum, blockid ) {
	var cache = new Date();
	var params = '_cache=' + cache.getFullYear() + cache.getMonth() + cache.getDate() + cache.getHours() + cache.getMinutes() + cache.getSeconds();
	params += '&' + 'page_id=' + pnum;
	$.ajax({
		url: 'http://www.shinwa-f.jp/bukken/',
		type: 'GET',
		cache: false,
		data: params,
		success: function( rsp ) {
			$('#' + blockid).html( rsp );
			block_size();
		}
	})
});

/* ===== イベント ================================================== */

//ドキュメントロード後
$(document).ready(function() {
	block_size();
	//winodwリサイズ
	$(window).bind( 'resize', function() {
		block_size();
	});
});
$(window).load(function() {
	block_size();
});